#ifndef __HW_H__
#define __HW_H__


#include "main.h"
#include "lcd.h"
#define TASK_MAX 5

typedef struct
{
    u8 keyMode;
    u8 keyState;
    u8 keyShotrFlag;
}keyFlag;

#define LCD_TAskTimer sysTimer[0]
#define ADC_TAskTimer sysTimer[1]
#define Key_TAskTimer sysTimer[2]
#define time_TAskTimer sysTimer[3]
#define LED_TAskTimer sysTimer[4]

#define LCD_TAskTImerPorid 50
#define Key_TAskTImerPorid 20
#define ADC_TAskTImerPorid 150
#define time_TAskTImerPorid 100
#define LED_TAskTImerPorid 200
void task_Run(void);
void sys_Init(void);
#endif

