#include "hw.h"
#include "usart.h"
#include "tim.h"
#include "adc.h"
#include "rtc.h"
#include "i2c_hal.h"
#include "stdio.h"
#include "string.h"
u32 sysTimer[TASK_MAX];
u8 rxData[64];
keyFlag key[4];
char view = 0;
char text[32];
RTC_TimeTypeDef RTC_time;
RTC_DateTypeDef RTC_Date;
int T_MAX = 40,Humi_MAX = 80;
int time = 1;
int frac = 1000;char choice = 0;
float k = 24.242424,b =-20,m = 0.008889,n = 1.111111;
float V  = 0;
int T = 0,humi = 0;
int PA1_frac = 0,PA7_frac = 0;
int num = 0;
char record[60][20];
char cnt = 0;
char LED_state = 0x00;
char send[64];
void LED_disp(char pin)
{
    GPIOC->ODR = ~(pin << 8);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
}
void save_eeprom(u8 addr,u8 data)
{
    I2CStart();
    I2CSendByte(0xA0);
    I2CWaitAck();
    I2CSendByte(addr);
    I2CWaitAck();
    I2CSendByte(data);
    I2CWaitAck();
    I2CStop();
    HAL_Delay(10);
}
u8 read_eeprom(u8 addr)
{
    u8 data = 0x00;
    I2CStart();
    I2CSendByte(0xA0);
    I2CWaitAck();
    I2CSendByte(addr);
    I2CWaitAck();
    I2CStart();
    I2CSendByte(0xA1);
    I2CWaitAck();
    data = I2CReceiveByte();
    I2CSendNotAck();
    I2CStop();
    HAL_Delay(10);
    return data;
    
}

void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
    if(htim -> Instance == TIM2)
    {
        if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
        {
            int val = TIM2->CCR2;
            TIM2->CCR2 = val + (80000000 / 80 / PA1_frac) * 0.5;
        }
    }
}
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
    if(htim -> Instance == TIM3)
    {
        if(htim -> Channel == HAL_TIM_ACTIVE_CHANNEL_2)
        {
            int val = TIM3->CNT;
            PA7_frac = 80000000 / 80 / val;
            TIM3->CNT = 0;
        }
    }
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if(htim->Instance == TIM17)
    {
        KeySc_task();
    }
}

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
{
    if(huart->Instance == USART1)
    {
        if(Size == 1)
        {
            if(rxData[0] == 'C')
            {
                sprintf(send,"T:%d-H:%d-%02d:%02d:%02d\r\n",T_MAX,Humi_MAX,RTC_time.Hours,RTC_time.Minutes,RTC_time.Seconds);   
                HAL_UART_Transmit(&huart1,(uint8_t*)send,strlen((char*)send),HAL_MAX_DELAY);
            }
            if(rxData[0] == 'T')
            {
                for(int i = 0;i < num;i++)
                {
                    HAL_UART_Transmit(&huart1,(uint8_t*)record[i],strlen(record[i]),HAL_MAX_DELAY);
                }
            }
            rxData[0] = '0';
        }
        HAL_UARTEx_ReceiveToIdle_DMA(&huart1,rxData,64);
    }
}
void sys_Init(void)
{
    LCD_Init();
    LCD_Clear(White);
    LCD_SetBackColor(White);
    LCD_SetTextColor(Black);
    LED_disp(0x00);
    HAL_UARTEx_ReceiveToIdle_DMA(&huart1,rxData,64);
    HAL_ADCEx_Calibration_Start(&hadc2,ADC_SINGLE_ENDED);
    HAL_TIM_IC_Start_IT(&htim3,TIM_CHANNEL_2);
    HAL_TIM_OC_Start_IT(&htim2,TIM_CHANNEL_2);
    HAL_TIM_Base_Start_IT(&htim17);
    HAL_ADC_Start(&hadc2);
    I2CInit();
    T_MAX = read_eeprom(0x00);
    Humi_MAX = read_eeprom(0x01);
    time = read_eeprom(0x02);
    for(int i = 0; i < 4; i++) 
    {
        PA1_frac |= read_eeprom(0x03 + i) << (i * 8);
    }
    
}

void KeySc_task()
{
    key[0].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0);
    key[1].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1);
    key[2].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2);
    key[3].keyState = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0);
    for(int i = 0;i < 4;i++)
    {
        switch(key[i].keyMode)
        {
            case 0:
            {
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 1;
                }
            }
            break;
            case 1:
            {
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 2;
                }
                else
                {
                    key[i].keyMode = 0;
                }
            }
            break;
            case 2:
            {
                if(key[i].keyState == 1)
                {
                    key[i].keyShotrFlag = 1;
                    key[i].keyMode = 0;
                }
            }
            
        }
    }
}

void ADC_Task(void)
{
    if(ADC_TAskTimer)return;
    ADC_TAskTimer = ADC_TAskTImerPorid;
    V = HAL_ADC_GetValue(&hadc2) * 3.3 / 4095;
    T = k * V + b;
    humi = m * PA7_frac + n;
    HAL_ADC_Start(&hadc2);
    
}

void key_Task(void)
{
    if(Key_TAskTimer)return;
    Key_TAskTimer = Key_TAskTImerPorid;
    if(key[0].keyShotrFlag)
    {
        if(view == 1)
        {
            save_eeprom(0x00,T_MAX);
            save_eeprom(0x01,Humi_MAX);
            save_eeprom(0x02,time);
            for(int i = 0;i < 4;i++)
            {
                save_eeprom(0x03+i,(PA1_frac>> (i * 8))&0xFF);
            }
        }
        view = !view;
        LCD_Clear(White);
        
        key[0].keyShotrFlag = 0;
    }
    if(key[1].keyShotrFlag)
    {
        if(view == 1)
        {
            choice ++;
            if(choice > 3)
            {
                choice = 0;
            }
        }
        key[1].keyShotrFlag = 0;
    }
    if(key[2].keyShotrFlag)
    {
        if(view == 1)
        {
            if(choice == 0)
            {
                T_MAX++;
                if(T_MAX > 60)
                {
                    T_MAX = 60;
                }
            }
            else if(choice == 1)
            {
                Humi_MAX += 5;
                if(Humi_MAX > 90)
                {
                    Humi_MAX = 90;
                }
            }
            else if(choice == 2)
            {
                time++;
                if(time > 5)
                {
                    time = 5;
                }
            }
            else if(choice == 3)
            {
                PA1_frac += 500;
                if(PA1_frac > 10000)
                {
                    PA1_frac = 10000;
                }
            }
        }
        key[2].keyShotrFlag = 0;
    }
    if(key[3].keyShotrFlag)
    {
        if(view == 1)
        {
            if(choice == 0)
            {
                T_MAX--;
                if(T_MAX < -20)
                {
                    T_MAX = -20;
                }
            }
            else if(choice == 1)
            {
                Humi_MAX -= 5;
                if(Humi_MAX < 10)
                {
                    Humi_MAX = 10;
                }
            }
            else if(choice == 2)
            {
                time--;
                if(time < 1)
                {
                    time = 1;
                }
            }
            else if(choice == 3)
            {
                PA1_frac -= 500;
                if(PA1_frac < 1000)
                {
                    PA1_frac = 1000;
                }
            }
        }
        key[3].keyShotrFlag = 0;
    }
    
}

void LCD_Disp_hight(u8 Line, u8 *ptr)
{
    if((48 + choice * 24) == Line)
    {
        LCD_SetTextColor(Green);
        LCD_DisplayStringLine(Line,ptr);
        LCD_SetTextColor(Black);
    }
    else
    {
        LCD_DisplayStringLine(Line,ptr);
    }
}

void LCD_Task(void)
{
    if(LCD_TAskTimer)return;
    LCD_TAskTimer = LCD_TAskTImerPorid;
    if(view == 0)
    {
        HAL_RTC_GetTime(&hrtc,&RTC_time,RTC_FORMAT_BIN);
        HAL_RTC_GetDate(&hrtc,&RTC_Date,RTC_FORMAT_BIN);
        sprintf(text,"        DATA        ");
        LCD_DisplayStringLine(Line0,(u8*)text);
        sprintf(text,"    Temp:%dC        ",T);
        LCD_DisplayStringLine(Line2,(u8*)text);
        sprintf(text,"    Humi:%d          ",humi);
        LCD_DisplayStringLine(Line3,(u8*)text);
        sprintf(text,"    %02d-%02d-%02d    ",RTC_time.Hours,RTC_time.Minutes,RTC_time.Seconds);;
        LCD_DisplayStringLine(Line4,(u8*)text);
    }
    else if(view == 1)
    {
        sprintf(text,"        DATA        ");
        LCD_Disp_hight(Line0,(u8*)text);
        sprintf(text,"    Temp_max:%dC        ",T_MAX);
        LCD_Disp_hight(Line2,(u8*)text);
        sprintf(text,"    Humi_max:%d          ",Humi_MAX);
        LCD_Disp_hight(Line3,(u8*)text);
        sprintf(text,"    time:%d    ",time);
        LCD_Disp_hight(Line4,(u8*)text);
        sprintf(text,"    frac:%.1fKHz    ",PA1_frac / 1000.0);
        LCD_Disp_hight(Line5,(u8*)text);
    }
        
}


void LED_Task(void)
{
    if(LED_TAskTimer)return;
    LED_TAskTimer = LED_TAskTImerPorid;
    if(T > T_MAX)
    {
       LED_state ^= 0x01; 
    }
    else
    {
        LED_state &= ~0x01;
    }
    if(humi > Humi_MAX)
    {
        LED_state ^= 0x02;
    }
    else
    {
        LED_state &= ~0x02;
    }
    LED_disp(LED_state);
}

void time_Task(void)
{
    if(time_TAskTimer)return;
    time_TAskTimer = time_TAskTImerPorid;
    cnt ++;
    if(cnt / 10 == time)
    {
        cnt = 0;
        sprintf(record[num++],"T:%d-H:%d-%02d:%02d:%02d\r\n",T,humi,RTC_time.Hours,RTC_time.Minutes,RTC_time.Seconds);
        LED_state ^= 0x04;
        if(num > 60)
        {
            num= 0;
        }
    }
}

void task_Run(void)
{
    LCD_Task();
    ADC_Task();
    key_Task();
    time_Task();
    LED_Task();
}
