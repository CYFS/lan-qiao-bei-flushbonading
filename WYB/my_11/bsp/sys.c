#include "sys.h"
#include "tim.h"
#include "stdio.h"
#include "adc.h"
#include "math.h"
u32 sysTimer[MAX_TIMER];
char view = 0;
u8 text[30];
keyFlag key[4]={0,0,0};
void taskInit()
{
    for(int i =0;i < MAX_TIMER;i++)
    {
        sysTimer[i] = 0;
    }
    LCD_Timer=LCD_TimerPeriod;
    Key_Timer=Key_TimerPeriod;
    Key_Flag_Timer=Key_Flag_TimerPeriod;
    ADC_Timer = ADC_TimerPeriod;
    PWM_OUT_Timer = PWM_OUT_TimerPeriod;
    LED_Timer = LED_TimerPeriod;
}
void LED_Disp(char state)
{
    GPIOC->ODR = ~(state << 8);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
}
void sysInit(void)
{
    LCD_Init();
    LCD_SetBackColor(Black);
    LCD_SetTextColor(White);
    LCD_Clear(Black);
    LED_Disp(0x00);
    taskInit();
    HAL_TIM_OC_Start_IT(&htim16,TIM_CHANNEL_1);
    HAL_TIM_OC_Start_IT(&htim17,TIM_CHANNEL_1);
    HAL_ADCEx_Calibration_Start(&hadc2, ADC_SINGLE_ENDED);
    
}

void SysTick_Handler(void)
{
    HAL_IncTick();
    for(char i = 0;i < MAX_TIMER;i++)
    {
        if(sysTimer[i])
        {
            sysTimer[i]--;
        }            
    }
}

float V_Value = 0;
char ModeSting[2][5]={"AUTO","MANU"};
char Mode = 0;
char PA6_Out=10;
char PA7_Out=10;
void LCD_Task(void)
{
    if(LCD_Timer)return;
    LCD_Timer = LCD_TimerPeriod;
    if(view == 0)
    {
        
        LCD_DisplayStringLine(Line0,(u8*)"      Data          ");
        sprintf((char*)text,"    V:%.2fV         ",V_Value);
        LCD_DisplayStringLine(Line2,text);
        sprintf((char*)text,"    Mode:%s       ",ModeSting[Mode]);
        LCD_DisplayStringLine(Line4,text);
    }
    else if(view == 1)
    {
        
        LCD_DisplayStringLine(Line0,(u8*)"      Para          ");
        sprintf((char*)text,"    PA6:%d%%          ",PA6_Out);
        LCD_DisplayStringLine(Line2,text);
        sprintf((char*)text,"    PA6:%d%%          ",PA7_Out);
        LCD_DisplayStringLine(Line4,text);
    }
}

void keyStateTask(void)
{
    if(Key_Timer)return;
    Key_Timer=Key_TimerPeriod;
    key[0].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0);
    key[1].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1);
    key[2].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2);
    key[3].keyState = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0);
    
    for(char i = 0;i < 4;i++)
    {
        switch(key[i].keyMode)
        {
            case 0:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 1;
                }
                break;
            case 1:
                if(key[i].keyState == 0)
                {
                    key[i].keyShortFlag = 1;
                    key[i].keyMode = 2;
                }
                else
                {
                    key[i].keyMode = 0;
                }
                break;
            case 2:
                if(key[i].keyState == 1)
                {
                   key[i].keyMode = 0;
                }
                break;
        }
    }
}
void keyFlagTask(void)
{
    if(Key_Flag_Timer)return;
    Key_Flag_Timer = Key_Flag_TimerPeriod;
    if(key[0].keyShortFlag)
    {
        view = !view;
        key[0].keyShortFlag = 0;
    }
    if(key[1].keyShortFlag)
    {
        if(view == 1)
        {
            PA6_Out += 10;
            if(PA6_Out > 90)
            {
                PA6_Out = 10;
            }
        }
        key[1].keyShortFlag = 0;
        TIM16->CCR1 = PA6_Out;
    }
    if(key[2].keyShortFlag)
    {
        if(view == 1)
        {
            PA7_Out += 10;
            if(PA7_Out > 90)
            {
                PA7_Out = 10;
            }
        }
        key[2].keyShortFlag = 0;
        TIM17->CCR1 = PA7_Out;
    }
    if(key[3].keyShortFlag)
    {
        Mode = !Mode;
        key[3].keyShortFlag = 0;
    }
}
void PWM_Out_task(void)
{
    if(PWM_OUT_Timer)return;
    PWM_OUT_Timer = PWM_OUT_TimerPeriod;
    if(Mode == 0)
    {
        
            
        
        if(fabs((double)V_Value-0.00)< 0.01)
        {
            PA6_Out=0;
            PA7_Out=0;
            TIM17->CCR1 = 0;
            TIM16->CCR1 = 0;
        }
        else if(fabs((double)V_Value - 3.3) < 0.01)
        {
            PA6_Out=100;
            PA7_Out=100;
            TIM17->CCR1 = 101;
            TIM16->CCR1 = 101;
        }
        else
        {
            PA6_Out = PA7_Out = (char)((double)V_Value/3.3 * 100); 
            TIM17->CCR1 = PA7_Out;
            TIM16->CCR1 = PA6_Out;
        }
        
    }
}
void ADC_Get_Task(void)
{
    if(ADC_Timer)return;
    ADC_Timer = ADC_TimerPeriod;
    HAL_ADC_Start(&hadc2);
    V_Value = (float)(HAL_ADC_GetValue(&hadc2)*3.3/4096);
    
}
char LED_State = 0x00;
void LED_Task(void)
{
    if(LED_Timer)return;
    LED_Timer = LED_TimerPeriod;
    if(Mode == 0)
    {
       LED_State |=0x01;
    }
    else
    {
        LED_State &= 0xFE;
    }
    if(view == 1)
    {
        LED_State |=0x02;
    }
    else
    {
        LED_State &= 0xFD;
    }
    LED_Disp(LED_State);
}
void taskRun(void)
{
    keyStateTask();
    keyFlagTask();
    ADC_Get_Task();
    LED_Task();
    PWM_Out_task();
    LCD_Task();
    
    
}
