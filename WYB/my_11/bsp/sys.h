#ifndef __SYS_H__
#define __SYS_H__
#include "main.h"
#include "stdbool.h"
#define MAX_TIMER 6
#define LCD_Timer sysTimer[0]
#define Key_Timer sysTimer[1]
#define Key_Flag_Timer sysTimer[2]
#define ADC_Timer sysTimer[3]
#define PWM_OUT_Timer sysTimer[4]
#define LED_Timer sysTimer[5]
#define LCD_TimerPeriod 100
#define Key_TimerPeriod 10
#define Key_Flag_TimerPeriod 10
#define ADC_TimerPeriod 20
#define PWM_OUT_TimerPeriod 10
#define LED_TimerPeriod 10

typedef struct
{
    char keyMode;
    bool keyState;
    bool keyShortFlag;
}keyFlag;

void taskRun(void);
void sysInit(void);


#endif
