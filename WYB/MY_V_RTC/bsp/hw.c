#include "hw.h"
#include "adc.h"
#include "usart.h"
#include "stdio.h"
#include "rtc.h"
#include "String.h"
#include "i2c - hal.h"
u32 sysTimer[Task_Max];

bool view = 0;
float k = 0.1f;
u8 text[30];
float V =0.0;
bool LEDFalg = 1;
RTC_TimeTypeDef sTime;
RTC_TimeTypeDef setTime;
void EEPROM_Write(uint8_t add,uint8_t data)
{
	I2CStart();
	I2CSendByte(0xA0);
	I2CWaitAck();
	
	I2CSendByte(add);
	I2CWaitAck();
	I2CSendByte(data);
	I2CWaitAck();
	I2CStop();
}

uint8_t EEPROM_Read(uint8_t add)
{
	uint16_t data;
	I2CStart();
	I2CSendByte(0xA0);
	I2CWaitAck();
	
	I2CSendByte(add);
	I2CWaitAck();
	
	I2CStart();
	I2CSendByte(0xA1);
	I2CWaitAck();
	data = I2CReceiveByte();
	I2CSendNotAck();
	I2CStop();
	
	return data;

}
void setRTC_A()
{
  RTC_AlarmTypeDef sAlarm = {0};
  sAlarm.AlarmTime.Hours = setTime.Hours;
  sAlarm.AlarmTime.Minutes = setTime.Minutes;
  sAlarm.AlarmTime.Seconds = setTime.Seconds;
  sAlarm.AlarmTime.SubSeconds = 0;
  sAlarm.AlarmMask = RTC_ALARMMASK_DATEWEEKDAY;
  sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
  sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
  sAlarm.AlarmDateWeekDay = 1;
  sAlarm.Alarm = RTC_ALARM_A;
  if (HAL_RTC_SetAlarm_IT(&hrtc, &sAlarm, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */
}
void EEPROM_Save_k(void)
{
    u8 tmp = k*10;
    EEPROM_Write(0x03,tmp);
}
void EEPROM_Read_k(void)
{
    u8 tmp = EEPROM_Read(0x03);
    k = tmp * 0.1;
}
void EEPORT_Save_Time(void)
{
    EEPROM_Write(0x00,setTime.Hours);
    HAL_Delay(10);
    EEPROM_Write(0x01,setTime.Minutes);
    HAL_Delay(10);
    EEPROM_Write(0x02,setTime.Seconds);
    HAL_Delay(10);
    
}
void EEPORT_Read_Time(void)
{
    setTime.Hours =  EEPROM_Read(0x00);
    HAL_Delay(10);
    setTime.Minutes = EEPROM_Read(0x01);
    HAL_Delay(10);
    setTime.Seconds = EEPROM_Read(0x02);
    HAL_Delay(10);
}

void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc) 
{
    char send[64];
    sprintf(send,"%.2f+%.1f+%02d%02d%02d\n",V,k,setTime.Hours,setTime.Minutes,setTime.Seconds);
     HAL_UART_Transmit(&huart1,(uint8_t*)send,strlen(send),HAL_MAX_DELAY);
}
keyFlag key[4];
void Key_Task(void)
{
    
    if(KeyTaskTimer)return;
    KeyTaskTimer = KeyTaskPriod;
    key[0].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0);
    key[1].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1);
    key[2].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2);
    key[3].keyState = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0);
    for(u8 i = 0;i < 4;i++)
    {
        switch(key[i].keyMode)
        {
            case 0:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode++;
                }
                break;
            case 1:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 2;
                }
                else
                {
                    key[i].keyMode = 0;
                }
                break;
            case 2:
                if(key[i].keyState == 1)
                {
                    key[i].keyShortFlag = 1;
                    key[i].keyMode = 0;
                }
                break;
        }
    }
}
void LED_Disp(u8 pin)
{
    GPIOC->ODR = ~(pin << 8);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
    
    
}

RTC_DateTypeDef sDate;

void taskInit(void)
{
    LEDTaskTimer = LEDTaskPriod;
    LCDTaskTimer = LCDTaskPriod;
    KeyTaskTimer = KeyTaskPriod;
}
u8 USART_RecTmp;
u8 USART_Data[64];
u8 USART_Pos = 0;
void sysInit(void)
{
    HAL_ADCEx_Calibration_Start(&hadc2,ENABLE);
    taskInit();
    HAL_UART_Receive_IT(&huart1,&USART_RecTmp,1); 
    LCD_Init();
    LCD_Clear(White);
	LCD_SetBackColor(White);
	LCD_SetTextColor(Black);
    LED_Disp(0x00);
    //EEPORT_Read_Time();
    setRTC_A();
    EEPROM_Read_k();
   
    
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    if(huart->Instance == USART1)
    {
        USARTTaskTimer = USAERTaskPriod;
        USART_Data[USART_Pos++] = USART_RecTmp;
        HAL_UART_Receive_IT(&huart1,&USART_RecTmp,1); 
    }
}
void USART_Task(void)
{
    if(USARTTaskTimer)return;
    if(!USART_Pos)return;
    USART_Data[USART_Pos] = '\0';
    u32 len = strlen((char*)USART_Data);

    if(len == 6)
    {
        #if 0  
        
       
        float K;
        char A;
        char B;
        sscanf((char*)USART_Data,"%c%f%c",&A,&K,&B);
        if(B == '\n' && A == 'k')
        {
            if(K > 0 && K < 1)
            {
                k=K;
                EEPROM_Save_k();
                HAL_UART_Transmit(&huart1,(uint8_t*)"ok\n",3,HAL_MAX_DELAY);
            } 
        }
    
        #else
    
        if(USART_Data[0] == 'k' && USART_Data[4] == '\\' && USART_Data[5] == 'n' && USART_Data[1] == '0' && USART_Data[2] == '.')
        {
            if(USART_Data[3] >='1' && USART_Data[3] <= '9')
            {
                
                k = (USART_Data[3] - '0') * 0.1;
                EEPROM_Save_k();
                HAL_UART_Transmit(&huart1,(uint8_t*)"ok\n",3,HAL_MAX_DELAY);
            }
        }
    
        #endif
    }
        
    
    memset((void*)USART_Data,0,64);
    USART_Pos = 0;
    
}
void ADC_Task(void)
{
    if(ADCTaskTimer)return;
    HAL_ADC_Start(&hadc2);
    ADCTaskTimer = ADCTaskPriod;
    V = hadc2.Instance->DR * 3.3 / 4095;
}
u16 time[3];
u8 choice = 0;

void LCD_Task(void)
{
    if(LCDTaskTimer)return;
    LCDTaskTimer = LCDTaskPriod;
    HAL_RTC_GetTime(&hrtc,&sTime,RTC_FORMAT_BIN);
    HAL_RTC_GetDate(&hrtc,&sDate,RTC_FORMAT_BIN);
    
    if(view == 0)
    {
        sprintf((void*)text,"   V1:%.2f              ",V);
        LCD_DisplayStringLine(Line4,text);
        sprintf((void*)text,"   k:%.1f              ",k);
        LCD_DisplayStringLine(Line5,text);
        sprintf((void*)text,"   LED:%s           ",LEDFalg == 0?"OFF":"ON");
        LCD_DisplayStringLine(Line6,text);
        sprintf((void*)text,"   T:%02d-%02d-%02d    ",sTime.Hours,sTime.Minutes,sTime.Seconds);
        LCD_DisplayStringLine(Line7,text);
    }
    else
    {
        sprintf((void*)text,"     Setting         ");
        LCD_DisplayStringLine(Line4,text);
        sprintf((void*)text,"                        ");
        LCD_DisplayStringLine(Line5,text);
        sprintf((void*)text,"                          ");
        LCD_DisplayStringLine(Line7,text);
        sprintf((void*)text,"    %02d-%02d-%02d    ",time[0],time[1],time[2]);
        LCD_DisplayStringLine(Line6,text);
    }
    
}

void keyFlag_Task(void)
{
    if(KeyFlagTaskTimer)return;
    KeyFlagTaskTimer = keyFalgTaskPriod;
    if(key[0].keyShortFlag)
    {
        LEDFalg = !LEDFalg;
        key[0].keyShortFlag = 0;
    }
    if(key[1].keyShortFlag)
    {
        view = !view;
        key[1].keyShortFlag = 0;
        if(view == 1)
        {
            time[0] = setTime.Hours;
            time[1] = setTime.Minutes;
            time[2] = setTime.Seconds;
        }
        else
        {
            setTime.Hours = time[0];
            setTime.Minutes = time[1];
            setTime.Seconds = time[2];
            //EEPORT_Save_Time();
            setRTC_A();
        }
    }
    if(key[2].keyShortFlag)
    {
        if(view == 1)
        {
            choice++;
            if(choice > 2)
            {
                choice = 0;
            }
        }
        key[2].keyShortFlag = 0;
    }
    if(key[3].keyShortFlag)
    {
        if(view == 1)
        {
            if(choice == 0)
            {
                time[0]++;
                if(time[0] > 23)
                {
                    time[0] = 0;
                }
            }
            else
            {
                time[choice]++;
                if(time[choice] > 59)
                {
                    time[choice] = 0;
                }
            }
        }
        key[3].keyShortFlag = 0;
    }
    
    
}

void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */
    for(u8 i = 0;i < Task_Max;i++)
    {
        if(sysTimer[i])
        {
            sysTimer[i]--;
        }
    }
  /* USER CODE END SysTick_IRQn 1 */
}
u8 LEDState = 0x00;
void LED_Task(void)
{
    if(LEDTaskTimer)return;
    LEDTaskTimer = LEDTaskPriod;
    if((V > (k * 3.3f)) && LEDFalg)
    {
        LEDState ^= 0x01;
    }
    else
    {
        LEDState = 0x00;
    }
    LED_Disp(LEDState);
}
void taskRun(void)
{
    Key_Task();
    keyFlag_Task();
    ADC_Task();
    USART_Task();
    LED_Task();
    LCD_Task();
}


