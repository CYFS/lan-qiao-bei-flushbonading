#ifndef __HW_H__
#define __HW_H__

#include "main.h"
#include "stdbool.h"
#include "lcd.h"

#define Task_Max 6
#define LCDTaskTimer sysTimer[0]
#define USARTTaskTimer sysTimer[1]
#define KeyTaskTimer sysTimer[2]
#define LEDTaskTimer sysTimer[3]
#define ADCTaskTimer sysTimer[4]
#define KeyFlagTaskTimer sysTimer[5]
#define LCDTaskPriod 100
#define USAERTaskPriod 10
#define KeyTaskPriod 10
#define LEDTaskPriod 200
#define ADCTaskPriod 150
#define keyFalgTaskPriod 15

typedef struct
{
    u8 keyMode;
    u8 keyState;
    u8 keyShortFlag;
}keyFlag;
void sysInit(void);
void taskRun(void);

#endif


