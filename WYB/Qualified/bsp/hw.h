#ifndef __HW_H__
#define __HW_H__
#include "stdbool.h"
#define LCD_TaskTimer sysTimer[0]
#define ADC_TaskTimer sysTimer[1]
#define key_TaskTimer sysTimer[2]
#define LED_TaskTimer sysTimer[3]
#define LCD_TaskTimerPoriod 50
#define ADC_TaskTimerPoriod 100
#define Key_TaskTimerPoriod 10
#define LED_TaskTimerPoriod 100

typedef struct
{
    char keyMode;
    bool keyState;
    bool keyShortFlag;
}keyFlag;
void Task_Run(void);
void sysInit(void);

#endif