#include "main.h"
#include "lcd.h"
#include "hw.h"
#include "stdio.h"
#include "adc.h"
#include "usart.h"
#include "math.h"
#include "string.h"
#define TASK_MAX 5
u32 sysTimer[TASK_MAX];
keyFlag key[4];
float V1 = 0 ,V2 = 0;
char view = 0;
char text[30];
float V1_Min = 1.2,V1_Max = 2.2,V2_Min = 1.4,V2_Max = 3.0;
int V1_detection = 0,V2_detection = 0;
int V1_Num = 0,V2_Num = 0;
char Choice = 1;
bool V1_Flag = 0,V2_Flag = 0;
extern DMA_HandleTypeDef hdma_usart1_rx;
void ADC_Task(void)
{
    if(ADC_TaskTimer)return;
    ADC_TaskTimer = ADC_TaskTimerPoriod;
    HAL_ADC_Start(&hadc1);
    V2 = HAL_ADC_GetValue(&hadc1) * 3.3 / 4095;
    HAL_ADC_Start(&hadc2);
    V1 = HAL_ADC_GetValue(&hadc2) * 3.3 / 4095;
}
void key_Task(void)
{
    if(key_TaskTimer)return;
    key_TaskTimer = Key_TaskTimerPoriod;
    key[0].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0);
    key[1].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1);
    key[2].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2);
    key[3].keyState = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0);
    for(char i = 0;i < 4;i++)
    {
        switch(key[i].keyMode)
        {
            case 0:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 1;
                }
                break;
            case 1:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 2;
                }
                else
                {
                    key[i].keyMode = 0;
                }
                break;
            case 2:
                if(key[i].keyState == 1)
                {
                    key[i].keyShortFlag = 1;
                    key[i].keyMode = 0;
                }       
                break;
        }
    }
    
}
void keyFlag_Task(void)
{
    if(key[0].keyShortFlag)
    {
        view ++;
        if(view > 2)
        {
            view = 0;
        }
        key[0].keyShortFlag = 0;
    }
    if(key[1].keyShortFlag)
    {
        if(view == 0)
        {
            V1_Num++;
            if((V1 > V1_Min) && (V1 < V1_Max))
            {
                V1_detection++;
                V1_Flag = 1;
            }
        }
        if(view == 1)
        {
            Choice++;
            if(Choice > 3)
            {
                Choice = 0;
                
            }
        }
        key[1].keyShortFlag = 0;
    }
    if(key[2].keyShortFlag)
    {
        if(view == 0)
        {
            V2_Num++;
            if((V2 > V2_Min) && (V2 < V2_Max))
            {
                V2_detection++;
                V2_Flag = 1;
            }
        }
        if(view == 1)
        {
            switch(Choice)
            {
                case 0:
                {
                    V1_Min += 0.2;
                    if(V1_Min > V1_Max)
                    {
                        V1_Min = V1_Max;
                    }
                }
                break;
                case 1:
                {
                    V1_Max += 0.2;
                    if(V1_Max > 3.0)
                    {
                        V1_Max = V1_Min;
                    }
                }
                break;
                case 2:
                {
                    V2_Min += 0.2;
                    if(V2_Min > V2_Max)
                    {
                        V2_Min = V2_Max;
                    }
                }
                break;
                case 3:
                {
                    V2_Max += 0.2;
                    if(V2_Max - 3.0 > 0)
                    {
                        V2_Max = V2_Min;
                    }
                }
                break;
            }
        }
        key[2].keyShortFlag = 0;
    }
    if(key[3].keyShortFlag)
    {
        if(view == 2)
        {
            V1_detection = 0;
            V1_Num = 0;
            V2_detection = 0;
            V2_Num = 0;
        }
        if(view == 1)
        {
           switch(Choice)
            {
                case 0:
                {
                    V1_Min -= 0.2;
                    if(V1_Min < 0)
                    {
                        V1_Min = V1_Max;
                    }
                }
                break;
                case 1:
                {
                    V1_Max -= 0.2;
                    if(V1_Max > 3.0)
                    {
                        V1_Max = V1_Min;
                    }
                }
                break;
                case 2:
                {
                    V2_Min -= 0.2;
                    if(V2_Min < 0)
                    {
                        V2_Min = V2_Max;
                    }
                }
                break;
                case 3:
                {
                    V2_Max -= 0.2;
                    if(V2_Max > 3.0)
                    {
                        V2_Max = V2_Min;
                    }
                }
                break;
            } 
        }
        key[3].keyShortFlag = 0;
    }
}

void LCD_Task(void)
{
    if(LCD_TaskTimer)return;
    LCD_TaskTimer = LCD_TaskTimerPoriod;
    if(view == 0)
    {
         sprintf(text,"       GOODS        ");
        LCD_DisplayStringLine(Line1,(u8*)text);        
        sprintf(text,"     R37:%.2fV      ",V1);
        LCD_DisplayStringLine(Line3,(u8*)text);
        sprintf(text,"     R38:%.2fV      ",V2);
        LCD_DisplayStringLine(Line4,(u8*)text);
    }
    else if(view == 1)
    {
       
        sprintf(text,"      STANDARD        ");
        LCD_DisplayStringLine(Line1,(u8*)text);        
        sprintf(text,"     SR37:%.1f-%.1f      ",V1_Min,V1_Max);
        LCD_DisplayStringLine(Line3,(u8*)text);
        sprintf(text,"     SR38:%.1f-%.1f      ",V2_Min,V2_Max);
        LCD_DisplayStringLine(Line4,(u8*)text);
    }
    else if(view == 2)
    {
       sprintf(text,"       PASS        ");
        LCD_DisplayStringLine(Line1,(u8*)text);        
        sprintf(text,"     PR37:%.1f%%     ",(double)V1_detection / V1_Num * 100);
        LCD_DisplayStringLine(Line3,(u8*)text);
        sprintf(text,"     PR38:%.1f%%     ",(double)V2_detection / V2_Num * 100);
        LCD_DisplayStringLine(Line4,(u8*)text); 
    }
}

void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */
    for(char i = 0;i < TASK_MAX;i++)
    {
        if(sysTimer[i])
        {
            sysTimer[i]--;
        }
    }
  /* USER CODE END SysTick_IRQn 1 */
}

uint8_t rec[30];
uint8_t send[64];
void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
{
    if(huart == &huart1)
    {
        if(Size == 3)
        {
            if(rec[0]== 'R'&& rec[1] == '3')
            {
                if(rec[2] == '7')
                {
                    sprintf((char*)send,"R37:%d:,%d,%.1f%%",V1_Num,V1_detection,(double)V1_detection / V1_Num * 100);
                    HAL_UART_Transmit(&huart1,send,strlen((char*)send),HAL_MAX_DELAY);
                }
                if(rec[2] == '8')
                {
                    sprintf((char*)send,"R38:%d:,%d,%.1f%%",V2_Num,V2_detection,(double)V2_detection / V2_Num * 100);
                    HAL_UART_Transmit(&huart1,send,strlen((char*)send),HAL_MAX_DELAY);
                }
            }
        }
        HAL_UARTEx_ReceiveToIdle_DMA(&huart1,rec,32);
        __HAL_DMA_DISABLE_IT(&hdma_usart1_rx,DMA_IT_HT);
    }
}

void sysInit(void)
{
    LCD_Init();
	LCD_Clear(Black);
	LCD_SetBackColor(Black);
	LCD_SetTextColor(White);
    HAL_UARTEx_ReceiveToIdle_DMA(&huart1,(uint8_t*)rec,32);
    __HAL_DMA_DISABLE_IT(&hdma_usart1_rx,DMA_IT_HT);
    HAL_ADCEx_Calibration_Start(&hadc2,ADC_SINGLE_ENDED);
    HAL_ADCEx_Calibration_Start(&hadc1,ADC_SINGLE_ENDED);
	
}
void LED_Disp(char pin)
{
    GPIOC->ODR = ~(pin << 8);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
    
}
char LED_State = 0x00;
char V1_Time = 0;
char V2_Time = 0;
void LED_Task(void)
{
    if(LED_TaskTimer)return;
    LED_TaskTimer = LED_TaskTimerPoriod;
    if(V1_Flag)
    {
        if(V1_Time < 10)
        {
            LED_State |= 0x01;
        }
        else
        {
            LED_State &= ~(0x01);
            V1_Time = 0;
            V1_Flag = 0;
        }
        V1_Time++;
    }
    if(V2_Flag)
    {
        if(V2_Time < 10)
        {
            LED_State |= 0x02;
        }
        else
        {
            LED_State &= ~(0x02);
            V2_Time = 0;
            V2_Flag = 0;
        }
        V2_Time++;
    }
    if(view == 0)
    {
        LED_State |= 0x04;
        LED_State &= ~(0x08);
        LED_State &= ~(0x10);
    }
    if(view == 1)
    {
        LED_State &= ~(0x04);
        LED_State |= 0x08;
        LED_State &= ~(0x10);
    }
    if(view == 2)
    {
        LED_State &= ~(0x04);
        LED_State &= ~(0x08);
        LED_State |= 0x10;
    }
    LED_Disp(LED_State);
}
void Task_Run(void)
{
    ADC_Task();
    key_Task();
    LED_Task();
    keyFlag_Task();
    LCD_Task();
}
