#ifndef __HW_H__
#define __HW_H__
#include "lcd.h"
#include "main.h"
#include "stdbool.h"
typedef struct
{
    char keyMode;
    bool keyState;
    bool keyShortFlag;
    int keyTime;
    bool keyLongFlag;
    
}ketFlag;

#define MAX_TASK 6

#define Key_TaskTimer sysTimer[0]
#define LED_TaskTimer sysTimer[1]
#define LCD_TaskTimer sysTimer[2]
#define ADC_TaskTimer sysTimer[3]
#define Speed_TaskTimer sysTimer[4]
#define Fre_TaskTimer sysTimer[5]


#define Key_TaskTimerPriod 10
#define LED_TaskTimerPriod 100
#define LCD_TaskTimerPriod 50
#define ADC_TaskTimerPriod 100
#define Speed_TaskTimerPriod 150
#define Fre_TaskTimerPriod 200

void sysInit(void);
void taskRun(void);


#endif
