#include "hw.h"
#include "adc.h"
#include "tim.h"
#include "stdio.h"
u32 sysTimer[MAX_TASK];

int new_R = 1;
int new_K = 1;
int choice = 0;
u32 time = 0;
ketFlag key[4] = {0,0,0,0};
char view = 0;
char text[30];
char PWM_Mode = 0;
float P = 0;
float speed = 0;
int R = 1,K = 1;
int N = 0;
float MH = 0,ML = 0;
bool ADC_State = 0;
int fra = 0;
void LED_Disp(unsigned char dsLED)
{
    HAL_GPIO_WritePin(GPIOC,GPIO_PIN_All,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOC,dsLED<<8,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
}
void taskInit(void)
{
    Key_TaskTimer = Key_TaskTimerPriod;
    LCD_TaskTimer = LCD_TaskTimerPriod;
    ADC_TaskTimer = ADC_TaskTimerPriod;
    LED_TaskTimer = LED_TaskTimerPriod;
    Speed_TaskTimer = Speed_TaskTimerPriod;
    Fre_TaskTimer = Fre_TaskTimerPriod;
}

void sysInit(void)
{
    LED_Disp(0x00);
    LCD_Init();
    
    LCD_SetBackColor(Black);
    LCD_SetTextColor(White);
    LCD_Clear(Black);
    taskInit();
    
    HAL_TIM_OC_Start_IT(&htim2,TIM_CHANNEL_2);
    __HAL_TIM_CLEAR_IT(&htim2,TIM_CHANNEL_2);
    HAL_TIM_IC_Start_IT(&htim3,TIM_CHANNEL_2);
    
    HAL_ADCEx_Calibration_Start(&hadc2,ADC_SINGLE_ENDED);
}


void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef * htim)
{
    if(htim->Instance == TIM3)
    {
        
        u16 crrl_val2 = HAL_TIM_ReadCapturedValue(&htim3,TIM_CHANNEL_2);
        
        fra = 1000000/crrl_val2;
        __HAL_TIM_SetCounter(&htim3,0);
        HAL_TIM_IC_Start(&htim3,TIM_CHANNEL_2);
        
    }
}
int PA1_Freq = 4000;
float PA1_Duty = 0.5;

void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
    static char Flag = 0;
    if(htim->Instance == TIM2)
    {
        if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
        {
            u32 value = HAL_TIM_ReadCapturedValue(htim,TIM_CHANNEL_2);
            if(Flag)
            {
                __HAL_TIM_SetCompare(&htim2,TIM_CHANNEL_2,value + (1000000 / PA1_Freq) * (1 - PA1_Duty));
            }
            else
            {
                __HAL_TIM_SetCompare(&htim2,TIM_CHANNEL_2,value + (1000000 / PA1_Freq) * (PA1_Duty));
            }
            Flag = !Flag;
        }
    }
}
void LCD_Task(void)
{
    if(LCD_TaskTimer)return;
    LCD_TaskTimer = LCD_TaskTimerPriod;
    if(view == 0)
    {
        sprintf(text,"        DATA        ");
        LCD_DisplayStringLine(Line1,(u8*)text);
        sprintf(text,"     M=%c            ",PWM_Mode == 0 ? 'L': 'H');
        LCD_DisplayStringLine(Line3,(u8*)text);
        sprintf(text,"     P=%02d%%           ",(int)(PA1_Duty * 100));
        LCD_DisplayStringLine(Line4,(u8*)text);
        sprintf(text,"     V=%.1f            ",speed);
        LCD_DisplayStringLine(Line5,(u8*)text);
        sprintf(text,"     %d           ",PA1_Freq);
        LCD_DisplayStringLine(Line6,(u8*)text);
    }
    else if (view == 1)
    {
        sprintf(text,"        PARA        ");
        LCD_DisplayStringLine(Line1,(u8*)text);
        sprintf(text,"     R=%d             ",new_R);
        LCD_DisplayStringLine(Line3,(u8*)text);
        sprintf(text,"     K=%d            ",new_K);
        LCD_DisplayStringLine(Line4,(u8*)text);
        sprintf(text,"                       ");
        LCD_DisplayStringLine(Line5,(u8*)text);
    }
    else if(view == 2)
    {
        sprintf(text,"        RECD        ");
        LCD_DisplayStringLine(Line1,(u8*)text);
        sprintf(text,"     N=%d             ",N);
        LCD_DisplayStringLine(Line3,(u8*)text);
        sprintf(text,"     MH=%.1f             ",MH);
        LCD_DisplayStringLine(Line4,(u8*)text);
        sprintf(text,"     ML=%.1f             ",ML);
        LCD_DisplayStringLine(Line5,(u8*)text);
    }
}
void KeySc_Task(void)
{
    if(Key_TaskTimer)return;
    Key_TaskTimer = Key_TaskTimerPriod;
    key[0].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0);
    key[1].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1);
    key[2].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2);
    key[3].keyState = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0);
    
    
    for(int i = 0;i < 4;i++)
    {
        switch(key[i].keyMode)
        {
            case 0:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 1;
                }
                break;
            case 1:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 2;
                }
                else
                {
                    key[i].keyMode = 0;
                }                    
                break;
            case 2:
                if(key[i].keyState == 1)
                {
                    if(key[i].keyTime < 200)
                    {
                        key[i].keyShortFlag = 1;
                    }
                    else
                    {
                        key[i].keyLongFlag =1;
                    }
                    key[i].keyTime = 0;
                     key[i].keyMode = 0;
                }
                else
                {
                    key[i].keyTime++;
                }
                break;                
        }           
    }
}

bool time_state =0;

void keyFlag_Task(void)
{
    if(key[0].keyShortFlag)
    {
        if(view == 1)
        {
            R= new_R;
            K = new_K;
        }
        view++;
        if(view > 2)
        {
            view = 0;
        }
        
        key[0].keyShortFlag = 0;
    }
    if(key[1].keyShortFlag)
    {
        if(view == 0)
        {
            if(!time_state)
            {
                time_state = 1;
                time = HAL_GetTick();
                PWM_Mode = !PWM_Mode;
                N++;
                
            }
                        
        }
        if(view == 1)
        {
            choice = !choice;
        }
        key[1].keyShortFlag = 0;
    }
    if(key[2].keyShortFlag)
    {
        if(view == 1)
        {
            if(choice == 1)
            {
                new_K++;
                if(new_K > 10)
                {
                    new_K = 1;
                }
            }
            else
            {
                new_R++;
                if(new_R > 10)
                {
                    new_R = 1;
                }
            }
            
        }
        key[2].keyShortFlag = 0;
    }
    if(key[3].keyShortFlag)
    {
        if(view == 1)
        {
            if(choice == 1)
            {
                new_K--;
                if(new_K < 1)
                {
                    new_K = 10;
                }
            }
            else
            {
                new_R--;
                if(new_R < 1)
                {
                    new_R = 10;
                }
            }
        }
        if(view == 0)
        {
            if(ADC_State)
            {
                ADC_State = 0;
            }
        }
        key[3].keyShortFlag = 0;
    }
    if(key[3].keyLongFlag)
    {
        if(!ADC_State)
        {
            ADC_State = 1;
        }
        key[3].keyLongFlag = 0;
    }    
}
float V = 0;
int count = 0;
void Fre_Task(void)
{
    if(Fre_TaskTimer)return;
    Fre_TaskTimer = Fre_TaskTimerPriod;
    if(HAL_GetTick() - time > 5000)
    {
        time_state = 0;
    }         
    if(time_state)
    {
        count++;
        if(PWM_Mode)
        {
            if(PA1_Freq < 8000)
            {
                PA1_Freq = 4000 + count * 200;
            }
        }
        else
        {
            if(PA1_Freq > 4000)
            {
                PA1_Freq = 8000 - count * 200;
            }
        }
    }
    else
    {
        count = 0;
    }
    
    

}
void ADC_Task(void)
{
    if(ADC_TaskTimer)return;
    ADC_TaskTimer = ADC_TaskTimerPriod;
    HAL_ADC_Start(&hadc2);
    V = HAL_ADC_GetValue(&hadc2) * 3.3 / 4095;    
    if(!ADC_State)
    {
        if(V <= 1)
        {
            PA1_Duty = 0.1;
            
        }
        else if(V >= 3)
        {
            PA1_Duty = 0.85;
        }
        else
        {
            PA1_Duty = (float)(0.375 * (V)  -0.275);
        }
    }
}

char LED_State = 0x00;
void LED_Task(void)
{
    if(LED_TaskTimer)return;
    LED_TaskTimer = LED_TaskTimerPriod;
    if(view == 0)
    {
        LED_State |= 0x01;
    }
    else
    {
        LED_State &= ~0x01;
    }
    if(time_state)
    {
        LED_State ^= 0x02;
    }
    else
    {
        LED_State &= ~0x02;
    }
    if(ADC_State)
    {
        LED_State |= 0x04;
    }
    else
    {
        LED_State &= ~0x04;
    }
    LED_Disp(LED_State);
}
void Speed_Task(void)
{
    if(Speed_TaskTimer)return;
    Speed_TaskTimer = Speed_TaskTimerPriod;
    speed = fra * 2 * 3.14 * R / 100 / K;
    if(PWM_Mode)
    {
        MH = MH > speed ? MH : speed;
    }
    else
    {
        ML = ML > speed ? ML : speed;
    }
}

void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */
    for(int i = 0;i < MAX_TASK;i++)
    {
        if(sysTimer[i])
        {
            sysTimer[i]--;
        }
    }
  /* USER CODE END SysTick_IRQn 1 */
}


void taskRun(void)
{
    KeySc_Task();
    keyFlag_Task();
    LED_Task();
    Fre_Task();
    Speed_Task();
    LCD_Task();
    ADC_Task();
    
    
}



