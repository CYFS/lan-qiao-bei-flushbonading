#ifndef __HARDWARE_H__
#define __HARDWARE_H__
#include "main.h"
#include "stdio.h"
#include "stdbool.h"
#include "adc.h"
#include "tim.h"
#include "lcd.h"
typedef struct
{
    char keyMode;
    bool keyState;
    bool keyShortFlag;

}keyFlag;
#define TIMER_MAX 5

#define KeyFlagTimer sysTimer[0]
#define KeyTimer sysTimer[1]
#define ADCTimer sysTimer[2]
#define LCDTimer sysTimer[3]
#define LEDTimer sysTimer[4]
#define ADCTaskPeriod 20
#define LEDTaskPeriod 100
#define LCDTaskPeriod 200
#define KeyFlagTaskPeriod 10
#define KeyTaskPeriod 10 

void taskRun();
void sysInit(void);


#endif
