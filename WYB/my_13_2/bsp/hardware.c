#include "hardware.h"
#include "math.h"
keyFlag key[4];
u32 sysTimer[TIMER_MAX];
float V;
float D;
int frac = 1000;
void SysTick_Handler(void)
{
  
    HAL_IncTick();
    for(int i = 0;i < TIMER_MAX;i++)
    {
        if(sysTimer[i])sysTimer[i]--;
    }
  
}
void LED_Disp(char pin)
{
    GPIOC->ODR = ~(pin << 8);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);

    
}
void taskInit(void)
{
    KeyFlagTimer = KeyFlagTaskPeriod;
}
void sysInit(void)
{
    LED_Disp(0x00);
    LCD_Init();
	LCD_Clear(White);
	LCD_SetBackColor(White);
	LCD_SetTextColor(Black);
    taskInit();
    HAL_ADCEx_Calibration_Start(&hadc2,ADC_SINGLE_ENDED);
    HAL_TIM_OC_Start_IT(&htim3,TIM_CHANNEL_2);
    
}
void KeyFlagTask(void)
{
    if(KeyFlagTimer)return;
    KeyFlagTimer = KeyFlagTaskPeriod;
    key[0].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0);
    key[1].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1);
    key[2].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2);
    key[3].keyState = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0);
    for(int i = 0;i < 4;i++)
    {
       switch(key[i].keyMode)
       {
           case 0:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 1;
                }
                break;
           case 1:
               if(key[i].keyState == 0)
               {
                   key[i].keyMode = 2;
                   key[i].keyShortFlag = 1;
               }
               else
               {
                   key[i].keyMode = 0;
               }
               break;
           case 2:
               if(key[i].keyState == 1)
               {
                   key[i].keyMode = 0;
               }
       }
    } 
}
bool keyLock = false;
void keyTask(void)
{
    if(KeyTimer)return;
    KeyTimer = KeyTaskPeriod;
    if(key[0].keyShortFlag)
    {
        if(!keyLock)
        {
            frac += 1000;
            if(frac > 10000)
            {
                frac = 10000;
            }
        }
        key[0].keyShortFlag = 0;
    }
    if(key[1].keyShortFlag)
    {
        if(!keyLock)
        {
            frac -= 1000;
            if(frac == 0)
            {
                frac = 1000;
            }
        }
        key[1].keyShortFlag = 0;
    }
    if(key[2].keyShortFlag)
    {
        keyLock = !keyLock;
        key[2].keyShortFlag = 0;
    }
    
}

void ADCTask(void)
{
    if(ADCTimer)return;
    ADCTimer = ADCTaskPeriod;
    HAL_ADC_Start(&hadc2);
    V = HAL_ADC_GetValue(&hadc2) / 4095.0 *3.3;
    if(V - 1 < 10e-3)
    {
        D = 0.4;
    }
    else if(V - 2 > 10e-3)
    {
        D = 0.8;
    }
    else
    {
        D = (V - 1) * 0.4 + 0.4;
    }
}
u8 text[30];
void LCDTask(void)
{
    if(LCDTimer)return;
    LCDTimer = LCDTaskPeriod;
    LCD_DisplayStringLine(Line1,(u8*)"        DATA        ");
    sprintf((char*)text,"   Volt:%.2fV       ",V);
    LCD_DisplayStringLine(Line3,text);
    sprintf((char*)text,"   D:%.0f%%       ",100*D);
    LCD_DisplayStringLine(Line4,text);
    sprintf((char*)text,"   F:%dHz       ",frac);
    LCD_DisplayStringLine(Line5,text);
    
    
}
void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
	static u8 Flag1 = 0;
	if(htim->Instance == TIM3)
	{
		if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
		{
			u16 Val = HAL_TIM_ReadCapturedValue(&htim3,TIM_CHANNEL_2);
			if(!Flag1)
			{
				__HAL_TIM_SetCompare(&htim3,TIM_CHANNEL_2,Val + (u16)(1000000 / frac) * (1 - D));
			}
			else
			{
				__HAL_TIM_SetCompare(&htim3,TIM_CHANNEL_2,Val + (u16)(1000000 / frac) *  D);
			}
            Flag1 = !Flag1;
		}
    }
		
}
char LED_State = 0x00;
void LEDTask(void)
{
    if(LEDTimer)return;
    LEDTimer = LEDTaskPeriod;
    if(keyLock)
    {
        LED_State |= 0x01;
    }
    else
    {
       LED_State &=~0x01; 
    }
    if(V - 1 > 0)
    {
        LED_State ^=0x02;
    }
    else
    {
        LED_State &= ~0x02;
    }
    LED_Disp(LED_State);
    
}
void taskRun()
{
    KeyFlagTask();
    keyTask();
    ADCTask();
    LCDTask();
    LEDTask();
}
