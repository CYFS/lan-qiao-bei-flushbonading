#ifndef __HW_H__
#define __HW_H__

#include "main.h"
#include "lcd.h"
#include "stdio.h"
#include "stdbool.h"

#define TIMER_MAX 5
#define LCD_TaskTime sysTimer[0]
#define keyFlag_TaskTime sysTimer[1]
#define LED_TaskTime sysTimer[2]
#define Fra_TaskTIme sysTimer[3]

#define LCD_TaskTimeProid 50
#define LED_TaskTimeProid 100
#define KeyFlag_TaskTimeProid 10
#define Fra_TaskTimeProid 80

typedef struct
{
    char keyMode;
    bool keyState;
    bool keyShortFlag;
}keyFlag;
void sysInit(void);
void taskRun(void);




#endif
