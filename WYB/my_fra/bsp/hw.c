#include "hw.h"
#include "usart.h"
#include "tim.h"
#include "i2c - hal.h"
u32 sysTimer[TIMER_MAX];
char text[36];
char N_1 = 2;
char N_2 = 2;
char PA1_Out = 0,PA2_Out = 0;
char recData[64];
void SysTick_Handler(void)
{
    HAL_IncTick();
    for(int i = 0;i < TIMER_MAX;i++)
    {
        if(sysTimer[i])
        {
            sysTimer[i]--;
        }
    }

}
void saveEEPROM(char addr,char data)
{
    I2CStart();
    I2CSendByte(0xA0);
    I2CWaitAck();
    I2CSendByte(addr);
    I2CWaitAck();
    I2CSendByte(data);
    I2CWaitAck();
    I2CStop();    
}
char readEEPROM(char addr)
{
    char data = 0x00;
    I2CStart();
    I2CSendByte(0xA0);
    I2CWaitAck();
    I2CSendByte(addr);
    I2CWaitAck();
    I2CStart();
    I2CSendByte(0xA1);
    I2CWaitAck();
    data = I2CReceiveByte();
    I2CSendNotAck();
    I2CStop();
    return data;
}

void LED_Disp(char pin)
{
    GPIOC->ODR = ~(pin << 8);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
    
}
void taskInit(void)
{
    LED_TaskTime = LED_TaskTimeProid;
    keyFlag_TaskTime = KeyFlag_TaskTimeProid;
    LCD_TaskTime = LCD_TaskTimeProid;
}

void sysInit(void)
{
    
    LCD_Init();
    LCD_Clear(White);
    LCD_SetBackColor(White);
    LCD_SetTextColor(Black);
    HAL_TIM_IC_Start_IT(&htim2,TIM_CHANNEL_2);
    HAL_TIM_IC_Start_IT(&htim2,TIM_CHANNEL_3);
    HAL_TIM_OC_Start_IT(&htim3,TIM_CHANNEL_1);
    HAL_TIM_OC_Start_IT(&htim3,TIM_CHANNEL_2);
    I2CInit();
    HAL_UARTEx_ReceiveToIdle_DMA(&huart1,(uint8_t*)recData,64);
    N_1 = readEEPROM(0x00);
    HAL_Delay(10);
    N_2 = readEEPROM(0x01);
    
}
keyFlag key[4];

void keySc_Task(void)
{
    if(keyFlag_TaskTime)return;
    keyFlag_TaskTime = KeyFlag_TaskTimeProid;
    key[0].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0);
    key[1].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1);
    key[2].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2);
    key[3].keyState = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0);
    for(int i = 0;i < 4;i++)
    {
        switch(key[i].keyMode)
        {
            case 0:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 1;
                }
                break;
            case 1:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 2;                   
                }
                else
                {
                    key[i].keyMode = 0;
                }
                break;
            case 2:
                if(key[i].keyState == 1)
                {
                    key[i].keyMode = 0;
                    key[i].keyShortFlag = 1;
                }
                break;
        }
    }
          
}

u32 PA6_Fra = 1000,PA7_Fra = 1000;
float PA6_Duty = 0.5f,PA7_Duty = 0.5f;
void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
    static char flag1 = 0,flag2 = 0;
    if(htim->Instance == TIM3)
    {
        u32 val = TIM3->CNT;
        if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)
        {
            if(PA1_Out)
            {
                if(!flag1)
                {
                    TIM3->CCR1 = val + 1000000 / PA6_Fra * (1 - PA6_Duty);
                }
                else
                {
                    TIM3->CCR1 = val + 1000000 / PA6_Fra * (PA6_Duty);
                }
                flag1 = !flag1;
            }
            else
            {
                HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,GPIO_PIN_RESET);
            }
        }
        if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
        {
            if(PA2_Out)
            {
                if(!flag2)
                {
                    TIM3->CCR2 = val + 1000000 / PA7_Fra * (1 - PA7_Duty);
                }
                else
                {
                    TIM3->CCR2 = val + 1000000 / PA7_Fra * (PA7_Duty);
                }
                flag2 = !flag2;
            }
            else
            {
                HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);
            }
        }
    }
        
    
}
u32 PA1_Fra = 0,PA2_Fra = 0;
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
    if(htim->Instance == TIM2)
    {
        if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
        {
            PA1_Fra = 1000000 / TIM2->CCR2 ;
            TIM2->CNT = 0;
        }
        if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_3)
        {
            PA2_Fra = 1000000 / TIM2->CCR2;
            TIM2->CNT = 0;
        }
    }
}
char setMode = 0;
char choice_Ch = 0;
void keyFlag_Task(void)
{
    if(key[0].keyShortFlag)
    {
        setMode = !setMode;
        key[0].keyShortFlag = 0;
    }
    if(key[1].keyShortFlag)
    {
        choice_Ch = !choice_Ch;
        key[1].keyShortFlag = 0;
    }
    if(key[2].keyShortFlag)
    {
        if(setMode == 0)
        {
            if(choice_Ch == 0)
            {
                N_1 --;
                if(N_1 < 1)
                {
                    N_1 = 1;
                }
                saveEEPROM(0x00,N_1);
            }
            if(choice_Ch == 1)
            {
                N_2 --;
                if(N_2 < 1)
                {
                    N_2 = 1;
                }
                saveEEPROM(0x01,N_2);
            }
        }
        key[2].keyShortFlag = 0;
    }
    if(key[3].keyShortFlag)
    {
        if(setMode == 0)
        {
            if(choice_Ch == 0)
            {
                N_1 ++;
                if(N_1 > 10)
                {
                    N_1 = 10;
                }
                saveEEPROM(0x00,N_1);
            }
            if(choice_Ch == 1)
            {
                N_2 ++;
                if(N_2 > 10)
                {
                    N_2 = 10;
                }
                saveEEPROM(0x01,N_2);
            }
        }
        key[3].keyShortFlag = 0;
    }
}


void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
{
    if(Size == 8)
    {
        int channel = 0;
        int N = 0;
        sscanf(recData,"SET:%d:%d",&channel,&N);
        if(channel != 0 && N != 0)
        {
            if(channel == 1)
            {
                if(N >=2 && N <= 10)
                {
                    N_1 = N;
                    saveEEPROM(0x00,N_1);
                }
            }
            if(channel == 2)
            {
                if(N >=2 && N <= 10)
                {
                    N_2 = N;
                    saveEEPROM(0x01,N_2);
                }
            }
        }
    }
}
char LED_State = 0x00;
void LED_Task(void)
{
    if(LED_TaskTime)return;
    LED_TaskTime = LED_TaskTimeProid;
    if(setMode)
    {
        LED_State |= 0x04;
    }
    else
    {
        LED_State &= ~0x04;
    }
    if(PA1_Out)
    {
        LED_State |= 0x01;
    }
    else
    {
        LED_State &= ~0x01;
    }
    if(PA2_Out)
    {
        LED_State |= 0x02;
    }
    else
    {
        LED_State &= ~0x02;
    }
    
    LED_Disp(LED_State);
    
    
}
void LCD_Task(void)
{
    sprintf(text,"  Channel(1):%dHz      ",PA1_Fra);
    LCD_DisplayStringLine(Line3,(u8*)text);
    sprintf(text,"  N(1):%d        ",N_1);
    LCD_DisplayStringLine(Line4,(u8*)text);
    sprintf(text,"  Channel(2):%dHz      ",PA2_Fra);
    LCD_DisplayStringLine(Line5,(u8*)text);
    sprintf(text,"  N(2):%d       ",N_2);
    LCD_DisplayStringLine(Line6,(u8*)text);
}

void Fra_Task(void)
{
    if(Fra_TaskTIme)return;
    Fra_TaskTIme = Fra_TaskTimeProid;
    if(PA1_Fra >= 50 && PA1_Fra <= 50000)
    {
        PA1_Out = 1;
    }
    else
    {
        PA1_Out = 0;
    }
    if(PA2_Fra >= 50 && PA2_Fra <= 50000)
    {
        PA2_Out = 1;
    }
    else
    {
        PA2_Out = 0;
    }
    if(PA1_Out)
    {
        
        PA6_Fra = PA1_Fra * N_1;
        PA6_Duty = 0.5f;
        
    }
    else
    {
        
        
        PA6_Duty = 0;
    }
    if(PA2_Out)
    {
        
        
        PA7_Fra = PA2_Fra * N_1;
        PA7_Duty = 0.5f;
        
    }
    else
    {             
        PA7_Duty = 0;
    }
}


void taskRun(void)
{
    LED_Task();
    Fra_Task();
    LCD_Task();
    keySc_Task();
    keyFlag_Task();
    
}


