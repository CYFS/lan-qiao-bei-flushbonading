#ifndef __HW_H__
#define __HW_H__


#include "main.h"
#include "lcd.h"
#include "stdbool.h"
#define MAX_TASK 4

#define LCD_TaskTimer sysTimer[0]
#define LED_TaskTimer sysTimer[1]
#define KeyFlag_TaskTimer sysTimer[2]


#define LCD_TaskTimerProid 50
#define LED_TaskTimerProid 100
#define KeyFlag_TaskTimerProid 10


typedef struct
{
    char keyMode;
    bool keyState;
    bool keyShortFlag;
}keyFlag;

void task_run(void);
void sysInit(void);
#endif
