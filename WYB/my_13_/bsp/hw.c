#include "hw.h"
#include "stdio.h"
#include "i2c_hal.h"
#include "usart.h"
#include <string.h>
#include "tim.h"
u32 sysTimer[MAX_TASK];
char view = 0;
char text[30];
char X = 0,Y = 0;
float X_Val = 1.0,Y_Val = 1.0;
char X_Num = 10,Y_Num = 10;
bool shop = 0;
char shop_time = 0;
void SysTick_Handler(void)
{
  
    HAL_IncTick();
    for(int i = 0;i < MAX_TASK;i++)
    {
        if(sysTimer[i])
        {
            sysTimer[i]--;
        }
    }
}

void LED_Disp(char pin)
{
    GPIOC->ODR = ~(pin << 8);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
}
void save_EEPROM(char addr,char byts)
{
    I2CStart();
    I2CSendByte(0xA0);
    I2CWaitAck();
    I2CSendByte(addr);
    I2CWaitAck();
    I2CSendByte(byts);
    I2CWaitAck();
    I2CStop();  
    HAL_Delay(10);    
}
char read_EEPROM(char addr)
{
    char byts = 0x00;
    I2CStart();
    I2CSendByte(0xA0);
    I2CWaitAck();
    I2CSendByte(addr);
    I2CWaitAck();
    I2CStart();
    I2CSendByte(0xA1);
    I2CWaitAck();
    byts = I2CReceiveByte();
    I2CSendNotAck();
    I2CStop();
    HAL_Delay(10);
    return byts;
}
char rec_data[64];
void sysInit(void)
{
    LCD_Init();
    LCD_Clear(White);
    LCD_SetBackColor(White);
    LCD_SetTextColor(Black);
    I2CInit();
    LED_Disp(0x00);
    X_Num = read_EEPROM(0x00);
    Y_Num = read_EEPROM(0x01);
    X_Val = read_EEPROM(0x02) / 10.0;
    Y_Val = read_EEPROM(0x03) / 10.0;
     HAL_UARTEx_ReceiveToIdle_DMA(&huart1,(uint8_t *)rec_data,30);
    HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_2);
}
keyFlag key[4];
void keySc_Task(void)
{
    if(KeyFlag_TaskTimer)return;
    KeyFlag_TaskTimer = KeyFlag_TaskTimerProid;
    
    key[0].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0);
    key[1].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1);
    key[2].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2);
    key[3].keyState = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0);
    for(int i = 0;i < 4;i++)
    {
        switch(key[i].keyMode)
        {
            case 0:
                if(key[i].keyState == 1)
                {
                    key[i].keyMode = 1;
                }
                break;
            case 1:
                if(key[i].keyState == 1)
                {
                    key[i].keyMode = 2;
                }
                else
                {
                    key[i].keyMode = 0;
                }
                break;
            case 2:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 0;
                    key[i].keyShortFlag = 1;
                }
                break;
        }
    }
    
}
void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
{
    if(Size == 1 && huart == &huart1)
    {
        if(rec_data[0] == '?')
        {
            char send_data[32];
            sprintf(send_data,"X:%.1f,Y:%.1f",X_Val,Y_Val);
            HAL_UART_Transmit(&huart1,(uint8_t*)send_data,strlen(send_data),HAL_MAX_DELAY);
            
        }
    }
    
}
char float_char(float num)
{
    num *=10;
    return (char)num;
}
void key_Task(void)
{
    if(key[0].keyShortFlag)
    {
        view++;
        if(view > 2)
        {
            view = 0;
        }
        key[0].keyShortFlag = 0;
    }
    if(key[1].keyShortFlag)
    {
        if(view == 0)
        {
            X++;
            if(X > X_Num)
            {
                X = X_Num;
            }
            
        }
        else if(view == 1)
        {
            X_Val += 0.1;
            //处理浮点型
            if(X_Val > 2.05)
            {
                X_Val = 1.0;
            }
            char X_Val_char = float_char(X_Val);
            save_EEPROM(0x02,X_Val_char);
        }
        else if(view == 2)
        {
            X_Num++;
            save_EEPROM(0x00,X_Num);
        }
        key[1].keyShortFlag = 0;
    }
    if(key[2].keyShortFlag)
    {
        if(view == 0)
        {
            Y++;
            if(Y > Y_Num)
            {
                Y = Y_Num;
            }
            
        }
        else if(view == 1)
        {
            Y_Val += 0.1;
            //处理浮点型
            //1.0 - 2.0
            if(Y_Val > 2.05)
            {
                Y_Val = 1.0;
            }
            char Y_Val_char = float_char(Y_Val);

            save_EEPROM(0x03,Y_Val_char);
            
        }
        else if(view == 2)
        {
            Y_Num++;
            save_EEPROM(0x01,Y_Num);
        }
        key[2].keyShortFlag = 0;
    }
    if(key[3].keyShortFlag)
    {
        shop = true;
        char send[64];
        float val = X * X_Val + Y * Y_Val;
        sprintf(send,"X:%d,Y:%d,Z:%.1f",X,Y,val);
        HAL_UART_Transmit(&huart1,(uint8_t*)send,strlen(send),HAL_MAX_DELAY);
       
        X_Num -= X;
        Y_Num -= Y;
         X = 0;
        Y = 0;
        save_EEPROM(0x00,X_Num);
        save_EEPROM(0x01,Y_Num);  
        key[3].keyShortFlag = 0;
    }
}


void LCD_Task(void)
{
    if(LCD_TaskTimer)return;
    LCD_TaskTimer = LCD_TaskTimerProid;
    if(view == 0)
    {
        sprintf(text,"        SHOP         ");
        LCD_DisplayStringLine(Line1,(u8*)text);
        sprintf(text,"     X:%d            ",X);
        LCD_DisplayStringLine(Line3,(u8*)text);
        sprintf(text,"     Y:%d            ",Y);
        LCD_DisplayStringLine(Line4,(u8*)text);
        
    }
    else if(view == 1)
    {
        sprintf(text,"        PRICE        ");
        LCD_DisplayStringLine(Line1,(u8*)text);
        sprintf(text,"     X:%.1f            ",X_Val);
        LCD_DisplayStringLine(Line3,(u8*)text);
        sprintf(text,"     Y:%.1f            ",Y_Val);
        LCD_DisplayStringLine(Line4,(u8*)text);
    }
    else if(view == 2)
    {
        sprintf(text,"        REP          ");
        LCD_DisplayStringLine(Line1,(u8*)text);
        sprintf(text,"     X:%d            ",X_Num);
        LCD_DisplayStringLine(Line3,(u8*)text);
        sprintf(text,"     Y:%d            ",Y_Num);
        LCD_DisplayStringLine(Line4,(u8*)text);
    }
    
    
    
}
char LED_State = 0x00;

void LED_Task(void)
{
    if(LED_TaskTimer)return;
    LED_TaskTimer = LED_TaskTimerProid;
    if(shop)
    {
        LED_State |= 0x01;        
        shop_time++;
        TIM2->CCR2 = 150;
        if(shop_time > 50)
        {
            TIM2->CCR2 = 25;
            shop_time = 0;
            shop = 0;
            LED_State &= ~0x01;
        }
        
    }
    if((X_Num == 0) && (Y_Num== 0))
    {
        LED_State ^= 0x02;
    }
    else
    {
        LED_State &= ~0x02;
    }
    LED_Disp(LED_State);    
}


void task_run(void)
{
    keySc_Task();
    key_Task();
    LCD_Task();
    LED_Task();
}



