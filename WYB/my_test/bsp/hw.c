#include "hw.h"
#include "usart.h"
#include "adc.h"
#include "tim.h"


u32 sysTimer[MAX_TASK];
KeyFlag key[4];
float V = 0;
char view = 0;
char text[32];
u32 T = 0;
bool uart_is = false;
double tmp_Vmax = 3.0f,tmp_Vmin = 1.0f;
double Vmax = 3.0f,Vmin = 1.0f;
char rec_data[64];
bool para_is = false,T_is = false;
char adc_state = 0;
bool t = 0;
void SysTick_Handler(void)
{
 
    HAL_IncTick();
    for(int i = 0;i < MAX_TASK;i++)
    {
        if(sysTimer[i])
        {
            sysTimer[i]--;
        }
    }

}





void LED_Disp(char pin)
{
    GPIOC->ODR = ~(pin << 8);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
}
void task_init()
{
    for(int i = 0;i < 4;i++)
    {
        key[i].keyMode = 0;
        key[i].keyShortFlag = 0;
    }
    LCD_TaskTimer = LCD_TaskTimerProid;
    LED_TaskTimer = LED_TaskTimerProid;
    ADC_TaskTimer = ADC_TaskTimerProid;
    KeyFlag_TaskTimer = KeyFlag_TaskTimerProid;
}
void sysInit(void)
{
    LCD_Init();
    LCD_Clear(White);
    LCD_SetBackColor(White);
    LCD_SetTextColor(Black);
    HAL_ADC_Start(&hadc2);
    task_init();
    LED_Disp(0x00);
    HAL_ADCEx_Calibration_Start(&hadc2,ADC_SINGLE_ENDED);
    HAL_UARTEx_ReceiveToIdle_DMA(&huart1,(uint8_t*)rec_data,64);
    
}
void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
{
    if(huart == &huart1)
    {
        if(Size == 7)
        {
            if(rec_data[1] == '.' && rec_data[3] == ',' && rec_data[5] == '.')
            {
                tmp_Vmax = rec_data[0] - '0' + (rec_data[2] - '0')/10.0; 
                tmp_Vmin = rec_data[4] - '0' + (rec_data[6] - '0')/10.0; 
            }
            else
            {
               uart_is = 1; 
                return;
            }
            if(tmp_Vmax > 3.3f || tmp_Vmin + 1 > tmp_Vmax)
            {
                uart_is = 1;
                return;
            }
            uart_is = 0;
            Vmax = tmp_Vmax;
            Vmin = tmp_Vmin;
        }
        else
        {
            uart_is = 1;
        }
    }
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if(htim == &htim2 && t)
    {
        T++;
    }
}

void KeySc_Task(void)
{
    if(KeyFlag_TaskTimer)return;
    KeyFlag_TaskTimer = KeyFlag_TaskTimerProid;
    key[0].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0);
    key[1].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1);
    key[2].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2);
    key[3].keyState = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0);
    
    
    for(int i = 0;i < 4;i++)
    {
        switch(key[i].keyMode)
        {
            case 0:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 1;
                }
                break;
            case 1:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 2;
                }
                else
                {
                    key[i].keyMode = 0;
                }
                break;
            case 2:
                if(key[i].keyState == 1)
                {
                    key[i].keyMode = 0;
                    key[i].keyShortFlag = 1;
                }
                break;
                
        }
    }
}

void Key_Task(void)
{
    if(key[0].keyShortFlag)
    {
        if(view == 0)
        {
            tmp_Vmax = Vmax;
            tmp_Vmin = Vmin;
        }
        else
        {
            if(tmp_Vmax < 3.3 && tmp_Vmin + 1 < tmp_Vmax)
            {
                Vmax = tmp_Vmax;
                Vmin = tmp_Vmin;
                para_is = 0;
            }
            else
            {
              para_is = 1;  
            }
        }
        view = !view;
        key[0].keyShortFlag = 0;  
    }
    if(key[1].keyShortFlag)
    {
        if(view == 1)
        {
            tmp_Vmax += 0.1;
            if(tmp_Vmax > 3.3)
            {
                tmp_Vmax = 0;
            }
        }
        key[1].keyShortFlag = 0;  
    }
    if(key[2].keyShortFlag)
    {
        tmp_Vmin += 0.1;
        if(tmp_Vmin > 3.3)
        {
            tmp_Vmin = 0;
        }
        key[2].keyShortFlag = 0;  
    }
    if(key[3].keyShortFlag)
    {
        key[3].keyShortFlag = 0;  
    }
}
char mode = 0;
void ADC_Task(void)
{
    if(ADC_TaskTimer)return;
    ADC_TaskTimer  = ADC_TaskTimerProid;
    
    V = HAL_ADC_GetValue(&hadc2) / 4095.0 * 3.3;
    HAL_ADC_Start(&hadc2);
    if(adc_state == 0)
    {
        if(V < Vmin)
        {
            T_is = 0;
            adc_state = 1; 
            HAL_TIM_Base_Start_IT(&htim2);
        }
    }
    else if(adc_state == 1)
    {
        
        if(V > Vmin)
        {
            T = 0;
            adc_state = 2;
        }
        
        
       
    }
    else if(adc_state == 2)
    {
        if(mode == 0)
        {
            if(V > Vmin && V < Vmax)
            {
                
                T_is = 1;
                t = 1;
            }
            else if(V < Vmin)
            {
                mode = 1;
            }
            else if(V > Vmax)
            {
               adc_state = 3; 
            }            
        }
        else if(mode == 1)
        {
           if(V > Vmin)
           {
               mode = 0;
               T = 0;
           }
       }
        
    }
    else if(adc_state == 3)
    {
        t = 0;
        T_is = 0;
        HAL_TIM_Base_Stop(&htim2);
        adc_state = 0;
    }
}

void LCD_Task(void)
{
    if(LCD_TaskTimer)return;
    LCD_TaskTimer = LCD_TaskTimerProid;
    if(view == 0)
    {
        sprintf(text,"      Data          ");
        LCD_DisplayStringLine(Line0,(u8*)text);
        sprintf(text," V:%.2fV             ",V);
        LCD_DisplayStringLine(Line2,(u8*)text);
        sprintf(text," T:%d               ",T);
        LCD_DisplayStringLine(Line3,(u8*)text);
    }
    else if(view == 1)
    {
        sprintf(text,"      Para          ");
        LCD_DisplayStringLine(Line0,(u8*)text);
        sprintf(text," Vmax:%.1fV             ",tmp_Vmax);
        LCD_DisplayStringLine(Line2,(u8*)text);
        sprintf(text," Vmin:%.1fV             ",tmp_Vmin);
        LCD_DisplayStringLine(Line3,(u8*)text);  
    }
}
char LED_State = 0x00;

void LED_Task(void)
{
    if(LED_TaskTimer)return;
    LED_TaskTimer = LED_TaskTimerProid;
    if(T_is)
    {
        LED_State |= 0x01;
    }
    else
    {
        LED_State &= ~0x01;
    }
    if(para_is)
    {
        LED_State |= 0x02;
    }
    else
    {
        LED_State &= ~0x02;
    }
    if(uart_is)
    {
        LED_State |= 0x04;
    }
    else
    {
        LED_State &= ~0x04;
    }
    LED_Disp(LED_State);
}
void taskRun(void)
{
    KeySc_Task();
    Key_Task();
    LED_Task();
    LCD_Task();
    ADC_Task();
}


