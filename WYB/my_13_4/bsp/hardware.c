#include "hardware.h"


u32 sysTimer[MAX_TIMER];

void SysTick_Handler(void)
{
  
    HAL_IncTick();
    for(int i = 0;i < MAX_TIMER;i++)
    {
        if(sysTimer[i])
        {
            sysTimer[i]--;
        }
    }

}
float V = 0;
u8 USART_Data[60];
u8 USART_Pos = 0;
u8 USART_RecTemp;

void taskInit(void)
{
    ADC_Timer = ADC_Timer_Porid;
    TIM_Timer = TIM_Timer_Porid;
    LCD_Timer = ADC_Timer_Porid;
    
}
void LED_Disp(char pin)
{
    GPIOC->ODR = ~(pin << 8);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
}
void sys_Init(void)
{
    LCD_Init();
    LCD_Clear(Black);
    LCD_SetBackColor(Black);
    LCD_SetTextColor(White);
    LED_Disp(0x00);

    taskInit();
    HAL_ADCEx_Calibration_Start(&hadc1,ADC_SINGLE_ENDED);
    HAL_TIM_IC_Start_IT(&htim3,TIM_CHANNEL_1);
    HAL_UART_Receive_IT(&huart1,&USART_RecTemp,1);
}
int crrl_val1 = 0;
int frq1;
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
    if(htim->Instance == TIM3)
    {
         crrl_val1 = HAL_TIM_ReadCapturedValue(htim,TIM_CHANNEL_1);
        __HAL_TIM_SetCounter(htim,0);
        frq1 = (80000000 / 80) / crrl_val1;
        HAL_TIM_IC_Start(htim,TIM_CHANNEL_1);
    }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart ->Instance == USART1)
	{
		USART_Timer = 10;
		USART_Data[USART_Pos ++] = USART_RecTemp;
		HAL_UART_Receive_IT(&huart1,&USART_RecTemp,1);
	}
}
void ADC_Task(void)
{
    if(ADC_Timer)return;
    ADC_Timer = ADC_Timer_Porid;
    HAL_ADC_Start(&hadc1);
    V = HAL_ADC_GetValue(&hadc1) / 4096.0 *3.3;
}

u8 test[30];
u8 count = 0;
void LCD_Task(void)
{
    if(LCD_Timer)return;
    LCD_Timer = LCD_Timer;
    LCD_DisplayStringLine(Line1,(u8*)"        DATA        ");
    sprintf((char*)test,"   V:%.2f          ",V);
    LCD_DisplayStringLine(Line3,test);
    sprintf((char*)test,"   F:%d          ",frq1);
    LCD_DisplayStringLine(Line4,test);
    sprintf((char*)test,"   N:%d          ",count);
    LCD_DisplayStringLine(Line5,test);
      
}
bool LED1_Flag = false;
bool LED2_Flag = false;
void USART_Task(void)
{
    if(USART_Timer)return;
    if(!USART_Pos)return;
    if(USART_Pos == 1)
    {
        if(USART_Data[0] == '#')
        {
            sprintf((char*)test,"%.2f",V);
            HAL_UART_Transmit(&huart1,test,strlen((char*)test),50);
            LED1_Flag = true;
            LED2_Flag = false; 
            count++;
        }
        else if(USART_Data[0] == '@')
        {
            sprintf((char*)test,"%d",frq1);
            HAL_UART_Transmit(&huart1,test,strlen((char*)test),50);
            LED1_Flag = true;
            LED2_Flag = false;
            count++;
        }
        else
        {
            sprintf((char*)test,"Error");
            HAL_UART_Transmit(&huart1,test,strlen((char*)test),50);
            LED2_Flag = true;
        }
            
    }
    else
    {
        sprintf((char*)test,"Error");
        HAL_UART_Transmit(&huart1,test,strlen((char*)test),50);\
        LED2_Flag = true;
    }
    USART_Pos = 0;
    memset(USART_Data,0,64);
    
}
int LED1_Timer = 0;
char LED_State = 0x00;
void LED_Task(void)
{
    if(LED_Timer)return;
    LED_Timer = LED_Timer_Porid;
    if(LED1_Flag)
    {
        LED1_Timer++;
        LED_State |= 0x01;
        if(LED1_Timer > 30)
        {
            LED_State &= ~(0x01);
            LED1_Flag = false;
            LED1_Timer = 0;
        }
    }
    if(LED2_Flag)
    {
        LED_State |= 0x02;
    }
    else
    {
        LED_State &= ~(0x02);
    }
    LED_Disp(LED_State);
}
void task_Run(void)
{
    ADC_Task();
    LCD_Task();
    USART_Task();
    LED_Task();
}

