#ifndef __SYS_H__
#define __SYS_H__
#include "main.h"
#include "lcd.h"
#include "adc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"
#include "stdio.h"
#include "string.h"
#include "stdbool.h"
#define MAX_TIMER 5
#define USART_Timer sysTimer[0]
#define LCD_Timer sysTimer[1]
#define ADC_Timer sysTimer[2]
#define TIM_Timer sysTimer[3]
#define LED_Timer sysTimer[4]
#define USART_Timer_Porid 10
#define LCD_Timer_Porid 100
#define ADC_Timer_Porid 10
#define TIM_Timer_Porid 10 
#define LED_Timer_Porid 100

void sys_Init(void);
void task_Run(void);




#endif
