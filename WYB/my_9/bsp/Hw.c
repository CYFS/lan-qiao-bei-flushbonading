#include "Hw.h"
#include "stdio.h"
#include "i2c - hal.h"
#include "string.h"
u32 sysTimer[TASK_MAX];
keyFalg key[4];
u8 text[32];
u8 time[5][3];
u8 timeSet[5][3];
u8 timeChoice = 0;
u8 TIMState[4][8]={"Running","Standby","Setting","Pause"};
u8 TIMChoice = 1;
bool LEDFalg = 0;
u8 setTime = 0;
bool setTimeFlag = 0;
bool temporaryFalg = 0;
void LED_Disp(u8 _LEDState)
{
    GPIOC->ODR = ~(_LEDState << 8);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
}
void EEPROM_SaveTime(char addr,u8 * Wtime)
{
    I2CStart();
    I2CSendByte(0xA0);
    I2CWaitAck();
    I2CSendByte(addr);
    I2CWaitAck();
    for(u8 i = 0;i < 3;i++)
    {
        I2CSendByte(Wtime[i]);
        I2CSendAck();
    }
    I2CStop();
}
void EEPROM_ReadTime(char addr,u8 * Rtime)
{
    I2CStart();
    I2CSendByte(0xA0);
    I2CWaitAck();
    I2CSendByte(addr);
    I2CWaitAck();
    I2CStart();
    I2CSendByte(0xA1);
    I2CWaitAck();
    Rtime[0] = I2CReceiveByte();
    I2CStop();
    HAL_Delay(1);
    I2CStart();
    I2CSendByte(0xA1);
    I2CWaitAck();
    Rtime[1] = I2CReceiveByte();
    I2CSendNotAck();
    I2CStop();
    HAL_Delay(1);
    I2CStart();
    I2CSendByte(0xA1);
    I2CWaitAck();
    Rtime[2] = I2CReceiveByte();
    I2CSendNotAck();
    I2CStop();
}
void SysTick_Handler(void)
{
  
    HAL_IncTick();
    for(u8 i = 0;i < TASK_MAX;i++)
    {
        if(sysTimer[i])
        {
            sysTimer[i]--;
        }
    }
}
void taskInit(void)
{
    LEDTaskTimer = LEDTaskTimerPorid;
    LCDTaskTimer = LCDTaskTimerPorid;
    keyTaskTimer = keyTaskTimerPorid;
    keyFlagTaskTimer = keyFlagTaskTimerPorid;
    
}
void sysInit(void)
{
    LCD_Init();	
	LCD_Clear(White);
	LCD_SetBackColor(White);
	LCD_SetTextColor(Black);
    LED_Disp(0x00);
    taskInit();
    EEPROM_ReadTime(0x00,time[0]);
    HAL_Delay(1);
    EEPROM_ReadTime(0x10,time[1]);
    HAL_Delay(1);
    EEPROM_ReadTime(0x20,time[2]);
    HAL_Delay(1);
    EEPROM_ReadTime(0x30,time[3]);
    HAL_Delay(1);
    EEPROM_ReadTime(0x40,time[4]);
    HAL_Delay(1);
    for(u8 i = 0;i < 5;i++)
    {
        strncpy((char*)timeSet[i],(char*)time[i],3);
    }
}

void keySc_Task(void)
{
    if(keyTaskTimer)return;
    keyTaskTimer = keyTaskTimerPorid;
    key[0].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0);
    key[1].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1);
    key[2].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2);
    key[3].keyState = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0);
      
    for(u8 i = 0;i < 4;i++)
    {
        switch(key[i].keyMode)
        {
            case 0:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 1;
                    key[i].keyTimer = 0;
                }
                break;
            case 1:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 2;
                    key[i].keyTimer++;
                }
                else
                {
                    key[i].keyMode = 0;
                }
                break;
            case 2:
                if(key[i].keyState == 1)
                {
                    if(key[i].keyTimer < 50)
                    {
                        key[i].keyShortFlag = 1;                        
                    }
                    
                    key[i].keyMode = 0;
                }
                else
                {
                    key[i].keyTimer++;   
                    if(key[i].keyTimer > 50)
                    {
                        key[i].keyLongFlag = 1;
                    }
                }
                break;
        }
    }
}
void keyFalg_Task(void)
{
    if(keyFlagTaskTimer)return;
    keyFlagTaskTimer = keyFlagTaskTimerPorid;
    if(key[0].keyShortFlag)
    {
        timeChoice++;
        if(timeChoice > 4)
        {
            timeChoice = 0;
        }
        key[0].keyShortFlag = 0;
    }
    if(key[1].keyShortFlag)
    {
        
        if(!setTimeFlag)
        {
            temporaryFalg = 1;
            TIMChoice = 2;
            setTime = 0;
            setTimeFlag = 1;
        }
        else
        {
            setTime++;              
        }
        key[1].keyShortFlag = 0;
    }
    if(key[2].keyShortFlag)
    {
        if(setTimeFlag)
        {
            timeSet[timeChoice][setTime]++;
        } 
        if(setTime == 0 && timeSet[timeChoice][setTime] > 23)
        {
            timeSet[timeChoice][setTime] = 0;
        }
        else if(setTime != 0 && timeSet[timeChoice][setTime] > 59)
        {
            timeSet[timeChoice][setTime] = 0;
        }
        key[2].keyShortFlag = 0;
    }
    if(key[3].keyShortFlag)
    {
        if(temporaryFalg)
        {
            if(TIMChoice == 2)
            {
                TIMChoice = 0;
            }
            else if(TIMChoice == 0)
            {
                TIMChoice = 3;
            }
            else if(TIMChoice == 3)
            {
                TIMChoice = 0;
            }
            key[3].keyShortFlag = 0;
        }else
        {
            if(TIMChoice == 1)
            {
                TIMChoice = 0;
            }
            else if(TIMChoice == 0)
            {
                TIMChoice = 3;
            }
            else if(TIMChoice == 3)
            {
                TIMChoice = 0;
            }
            key[3].keyShortFlag = 0;
        }
    }
    if(key[1].keyLongFlag)
    {
        setTimeFlag = 0;
        strncpy((char*)time[timeChoice],(char*)timeSet[timeChoice],3);
        u8 addr = timeChoice << 4;
        EEPROM_SaveTime(addr,time[timeChoice]);
        key[1].keyLongFlag = 0;
        TIMChoice = 0;
    }
    if(key[2].keyLongFlag)
    {
        if(setTimeFlag)
        {
            timeSet[timeChoice][setTime]++;
        }
        if(setTime == 0 && timeSet[timeChoice][setTime] > 23)
        {
            timeSet[timeChoice][setTime] = 0;
        }
        else if(setTime != 0 && timeSet[timeChoice][setTime] > 59)
        {
            timeSet[timeChoice][setTime] = 0;
        }
        key[2].keyLongFlag = 0;
    }
    if(key[3].keyLongFlag)
    {
        TIMChoice = 1;
        temporaryFalg = 0;
        key[3].keyLongFlag = 0;
    }
}


void LCD_Task(void)
{
    if(LCDTaskTimer)return;
    LCDTaskTimer = LCDTaskTimerPorid;
    if(temporaryFalg == 1)
    {
        sprintf((char*)text,"   No %d            ",timeChoice + 1);
        LCD_DisplayStringLine(Line1,text);
        sprintf((char*)text,"      %02d:%02d:%02d    ",timeSet[timeChoice][0],timeSet[timeChoice][1],timeSet[timeChoice][2]);
        LCD_DisplayStringLine(Line3,text);
        sprintf((char*)text,"        %s      ",TIMState[TIMChoice]);
        LCD_DisplayStringLine(Line5,text);
        return;
    }
    sprintf((char*)text,"   No %d            ",timeChoice + 1);
    LCD_DisplayStringLine(Line1,text);
    sprintf((char*)text,"      %02d:%02d:%02d    ",time[timeChoice][0],time[timeChoice][1],time[timeChoice][2]);
    LCD_DisplayStringLine(Line3,text);
    sprintf((char*)text,"        %s      ",TIMState[TIMChoice]);
    LCD_DisplayStringLine(Line5,text);
}

u8 LEDState;
void LED_Task(void)
{
    if(LEDTaskTimer)return;
    LEDTaskTimer = LEDTaskTimerPorid;
    if(LEDFalg)
    {
        LEDState ^= 0x01;
    }
    else
    {
        LEDState = 0x00;
    }
    LED_Disp(LEDState);
    
}
void taskRun(void)
{
    keySc_Task();
    keyFalg_Task();
    LED_Task();
    LCD_Task();
}

