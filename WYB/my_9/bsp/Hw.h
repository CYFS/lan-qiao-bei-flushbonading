#ifndef __HW_H__
#define __HW_H__
#include "main.h"
#include "lcd.h"
#include "stdbool.h"

#define TASK_MAX 5
#define keyTaskTimer sysTimer[0]
#define LEDTaskTimer sysTimer[1]
#define LCDTaskTimer sysTimer[2]
#define keyFlagTaskTimer sysTimer[3]

#define keyTaskTimerPorid 10
#define LEDTaskTimerPorid 500
#define LCDTaskTimerPorid 100
#define keyFlagTaskTimerPorid 15






typedef struct
{
    u8 keyMode;
    bool keyState;
    bool keyShortFlag;
    bool keyLongFlag;
    u16 keyTimer;
}keyFalg;


void taskRun(void);
void sysInit(void);



#endif
