#include "hw.h"
#include "stdio.h"
u32 sysTimer[Task_MAX];

int Sleep_Timer = 5000;
keyFlag key[4];
extern DMA_HandleTypeDef hdma_usart1_rx;
char rec[30];
char view = 0;
char text[30];
float temp = 0;
int gear = 0;
char mode = 0;
char mode_choice[2][5] = {"Auto","Manu"};
void LED_Diap(char pin)
{
    GPIOC->ODR = ~(pin << 8);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
    
}
void sysInit(void)
{
    LCD_Init();
    LCD_Clear(White);
    LED_Diap(0x00);
	LCD_SetBackColor(White);
	LCD_SetTextColor(Black);
    HAL_ADCEx_Calibration_Start(&hadc2,ADC_SINGLE_ENDED);
    HAL_UARTEx_ReceiveToIdle_DMA(&huart1,(u8*)rec,30);
}
void add_gear(void)
{
    if(mode == 1)
    {
        gear++;
        if(gear > 2)
        {
            gear = 0;
        }
    }
}
void dec_gear(void)
{
    if(mode == 1)
    {
        gear--;
        if(gear < 0)
        {
            gear = 2;
        }
    }
}
char uartFlag = 0;
void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
{
    uartFlag = 1;
    Sleep_Timer = 5000;
    if(Size == 2)
    {
        if(rec[0] == 'B')
        {
            if(view == 0)
            {
                if(rec[1] == '1')
                {
                    mode = !mode;
                }
                else if(rec[1] == '2')
                {
                    add_gear();
                }else if(rec[1] == '3')
                {
                    dec_gear();
                }
                else
                {
                    HAL_UART_Transmit(&huart1,(u8*)"NULL",4,HAL_MAX_DELAY);
                }
            }
            else
            {
                if(rec[1] == '1' || rec[1] == '2' || rec[1] == '3')
                {
                    view = 0;
                }
                else
                {
                    HAL_UART_Transmit(&huart1,(u8*)"NULL",4,HAL_MAX_DELAY);
                }
            }
        }
        else
        {
            HAL_UART_Transmit(&huart1,(u8*)"NULL",4,HAL_MAX_DELAY);
        }
    }
    else
    {
        HAL_UART_Transmit(&huart1,(u8*)"NULL",4,HAL_MAX_DELAY);
    }
}    


void SysTick_Handler(void)
{
    HAL_IncTick();
    if(Sleep_Timer)
    {
        Sleep_Timer--;
    }
    for(int i = 0;i < Task_MAX;i++)
    {
        if(sysTimer[i])
        {
            sysTimer[i]--;
        }
    }
}
void keyScTask(void)
{
    if(Key_TaskTimer)return;
    Key_TaskTimer = Key_TaskTimeProid;
    key[0].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0);
    key[1].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1);
    key[2].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2);
    key[3].keyState = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0);
    
    
    for(int i = 0;i < 4;i++)
    {
        switch(key[i].keyMode)
        {
            case 0:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 1;
                }
                break;
            case 1:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 2;
                }
                else
                {
                    key[i].keyMode =0;
                }
                break;
            case 2:
                if(key[i].keyState == 1)
                {
                    key[i].keyShortFlag = 1;
                    key[i].keyMode = 0;
                }
                break;
        }
    }
}

void LCD_Task(void)
{
    if(LCD_TaskTimer)return;
    LCD_TaskTimer = LCD_TaskTierProid;
    if(view == 0)
    {
        sprintf(text,"        DATA        ");
        LCD_DisplayStringLine(Line1,(u8*)text);
        sprintf(text,"     TEMP:%4.1f      ",temp);
        LCD_DisplayStringLine(Line3,(u8*)text);
        sprintf(text,"     MODE:%s      ",mode_choice[mode]);
        LCD_DisplayStringLine(Line4,(u8*)text);
        sprintf(text,"     GEAR:%d      ",gear);
        LCD_DisplayStringLine(Line5,(u8*)text);
        
    }
    else if(view == 1)
    {
        sprintf(text,"                    ");
        LCD_DisplayStringLine(Line1,(u8*)text);
        sprintf(text,"                    ");
        LCD_DisplayStringLine(Line3,(u8*)text);
        sprintf(text,"     SLEEPING       ");
        LCD_DisplayStringLine(Line4,(u8*)text);
        sprintf(text,"     TEMP:%4.1f      ",temp);
        LCD_DisplayStringLine(Line5,(u8*)text);
    }
        
}
float V = 0;
void ADC_Task(void)
{
    if(ADC_TaskTimer)return;
    ADC_TaskTimer = ADC_TaskTimeProid;
    HAL_ADC_Start(&hadc2);
    V = HAL_ADC_GetValue(&hadc2) * 3.3 / 4095;
    if(V >= 1 && V <= 3)
    {
        temp = 20 + 10 * (V - 1);
    }
    else if(V > 3)
    {
        temp = 40;
    }
    else
    {
        temp = 20;
    }
    if(mode == 0)
    {
        if(temp < 25)
        {
            gear = 0;
        }
        else if(temp > 30)
        {
            gear = 2;         
        }
        else
        {
            gear = 1;
        }
    }
    
}
void keyFalgTask(void)
{
    if(key[0].keyShortFlag)
    {
         Sleep_Timer = 5000;
        if(view == 0)
        {
            mode = !mode;
        }
        key[0].keyShortFlag = 0;
    }
    if(key[1].keyShortFlag)
    {
         Sleep_Timer = 5000;
        if(view == 0)
        {
            add_gear();
        }
        
        key[1].keyShortFlag = 0;
    }
    if(key[2].keyShortFlag)
    {
         Sleep_Timer = 5000;
       
        if(view == 0)
        {
            dec_gear();
        }
        
        key[2].keyShortFlag = 0;
    }
    if(key[3].keyShortFlag)
    {
        key[3].keyShortFlag = 0;
    }
    
}

char LED_State = 0x00;
char LEDTime = 0;
void LED_Task(void)
{
    if(LED_TaskTimer)return;
    LED_TaskTimer = LED_TaskTimeProid;
    if(gear == 0)
    {
        LED_State |= 0x01;
        LED_State &= ~0x02;
        LED_State &= ~0x04;
    }
    else if(gear == 1)
    {
        LED_State &= ~0x01;
        LED_State |= 0x02;
        LED_State &= ~0x04;
    }
    else if(gear == 2)
    {
        LED_State &= ~0x01;
        LED_State &= ~0x02;
        LED_State |= 0x04;
    }
    if(uartFlag)
    {
        LED_State |= 0x08;
        LEDTime++;
        if(LEDTime > 30)
        {
            uartFlag = 0;
            LEDTime = 0;
        }
    }
    else
    {
        LED_State &= ~0x08;
    }
    LED_Diap(LED_State);
    
}

void taskRun(void)
{
    keyScTask();
    keyFalgTask();
    ADC_Task();
    LED_Task();
    LCD_Task(); 
    if(!Sleep_Timer)
    {
        view = 1;
    }else
    {
        view = 0;
    }
    
}

