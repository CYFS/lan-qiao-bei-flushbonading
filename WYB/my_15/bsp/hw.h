#ifndef __HW_H__
#define __HW_H__

#include "lcd.h"
#include "adc.h"
#include "usart.h"
#include "stdbool.h"

#define Task_MAX 5
#define LCD_TaskTimer sysTimer[0]
#define ADC_TaskTimer sysTimer[1]
#define Key_TaskTimer sysTimer[2]
#define LED_TaskTimer sysTimer[3]
#define Sleep_TaskTimer sysTimer[4]
#define LCD_TaskTierProid 50
#define ADC_TaskTimeProid 150
#define Key_TaskTimeProid 10
#define LED_TaskTimeProid 100
#define Sleep_TaskTimeProid 200

typedef struct{
    char keyMode;
    bool keyState;
    bool keyShortFlag;
}keyFlag;

void taskRun(void);

void sysInit(void);
#endif


