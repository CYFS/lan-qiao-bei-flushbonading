#include "sys.h"
u32 sysTimer[MAX_TIMER];
u8 view = 0;
u8 text[25];
u8 inputPassword[4] ="@@@";
u8 password[4]="123";
int frac = 2000;
u8 D = 10;
void taskInit(void)
{
    for(u8 i = 0;i < MAX_TIMER;i++)
    {
        sysTimer[i]=0;
    }
    LCDTaskTimer = LCDTaskPeriod;
	KEYTaskTimer = KEYTaskPeriod;
	USARTTTaskTimer = USARTTaskPeriod;
	LEDTaskTimer = LEDTaskPeriod;
}
void SysTick_Handler(void)
{
  HAL_IncTick();

	for (u8 i =0; i < MAX_TIMER ; i ++)
	{
		if(sysTimer[i])
			sysTimer[i] --;
	}
}
void Led_Display (u8 state)
{
	GPIOC->ODR = ~(state << 8);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
}
u8 USART_RecTemp;
u8 USART_RecPos;
u8 USART_RecData[64];
void Sys_Init(void)
{
	LCD_Init();
	LCD_Clear(Black);                                                            
	LCD_SetBackColor(Black);
	LCD_SetTextColor(White);
	taskInit();
	Led_Display(0x00);
	HAL_TIM_OC_Start_IT(&htim2,TIM_CHANNEL_2);
	HAL_UART_Receive_IT(&huart1,&USART_RecTemp,1);
}

void taskRun(void)
{
    
    keyTask();
    keyFlagTask();
    LCD_Task();
    LED_Task();
    USAERT_Task();
   
}
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart ->Instance == USART1)
	{
		USARTTTaskTimer = 10;
		USART_RecData[USART_RecPos ++] = USART_RecTemp;
		HAL_UART_Receive_IT(&huart1,&USART_RecTemp,1);
	}
}
void USAERT_Task(void)
{
    if(USARTTTaskTimer)return;
    if(!USART_RecPos)return;
    USART_RecData[USART_RecPos]='\0';
    char oldPassword[4];
    char newPassword[4];
    if(strlen((char*)USART_RecData) == 7)
    {
        
        sscanf((char*)USART_RecData,"%3s-%3s",oldPassword,newPassword);
        if(strcmp((char*)password,oldPassword) == 0)
        {
            char i = 0;
            for(i = 0;i < 3;i++)
            {
                if(newPassword[i]<'0'||newPassword[i]>'9') break;
            }
            if(i == 3)
            {
                strcpy((char*)password,newPassword);
            }
           
        }
    }
    memset(USART_RecData,0,64);
	USART_RecPos = 0;
}
void PWM_1K_OUT(void)
{
    __HAL_TIM_SetAutoreload(&htim2,1000-1);
  __HAL_TIM_SetCompare(&htim2,TIM_CHANNEL_2,500);
	HAL_TIM_GenerateEvent(&htim2, TIM_EVENTSOURCE_UPDATE);
}
void PWM_2K_OUT(void)
{
  __HAL_TIM_SetAutoreload(&htim2,500-1);
  __HAL_TIM_SetCompare(&htim2,TIM_CHANNEL_2,50);
	HAL_TIM_GenerateEvent(&htim2, TIM_EVENTSOURCE_UPDATE);
  
}
keyFlag key[4]={0,0,0,0};
void keyTask(void)
{   
    if(KEYTaskTimer)return;
    KEYTaskTimer=KEYTaskPeriod;
 //读取按钮状态
    key[0].keyState = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_0);
    key[1].keyState = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_1);
    key[2].keyState = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_2);
    key[3].keyState = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0);

    for (int i = 0; i < 4; i++)
    {
        switch (key[i].keyMode)
        {
            case 0:
                // 模式0：按键空闲状态
                if (key[i].keyState == 0) 
                {
                    // 如果按键被按下
                    key[i].keyMode = 1;
                    //key[i].keyTime = 0;  // 重置按键时间
                }
                break;
            case 1:
                // 模式1：按键按下检测中
                if (key[i].keyState == 0)
                {
                    // 如果按键仍然被按下
                    key[i].keyMode = 2;
                    key[i].keyTime++;//按下时间加一
                }
                else
                {
                    // 如果按键被释放
                    key[i].keyMode = 0;
                }
                break;
            case 2:
                // 模式2：按键释放和长按检测中
                if (key[i].keyState == 1) 
                {
                    // 如果按键被释放
                    key[i].keyMode = 0;
                    if (key[i].keyTime < 70) 
                    {
                        // 如果按键时间短，标记为短按
                        key[i].keyShortFlag = 1;
                    }
                } 
                else
                {
                    // 如果按键仍然被按下
                    key[i].keyTime++;
                    if (key[i].keyTime > 70)//因为定时器是10ms进一次，所以就是70*10=700ms
                    {
                        // 如果按键时间长，标记为长按
                        key[i].keyLongFlag = 1;
                    }
                }
                break;
        }
    }
}
char count = 0;
bool LED2Flag = false;
bool LED1Flag = false;
void keyFlagTask(void)
{
    if(keyFlagTaskTimer)return;
    keyFlagTaskTimer = keyFlagTaskPeriod;
    if(key[0].keyShortFlag)
    {
        if(view == 0)
        {
            if(inputPassword[0] == '@')
            {
                inputPassword[0]='0';
            }
            else
            {
                inputPassword[0]++;
            }
            if(inputPassword[0] > '9')
            {
                inputPassword[0] = '0';
            }
        }
        key[0].keyShortFlag = 0;
    }
    if(key[1].keyShortFlag)
    {
        if(view == 0)
        {
            if(inputPassword[1] == '@')
            {
                inputPassword[1]='0';
            }
            else
            {
                inputPassword[1]++;
            }
            if(inputPassword[1] > '9')
            {
                inputPassword[1] = '0';
            }
        }
        key[1].keyShortFlag = 0;
    }
    if(key[2].keyShortFlag)
    {
        if(view == 0)
        {
            if(inputPassword[2] == '@')
            {
                inputPassword[2]='0';
            }
            else
            {
                inputPassword[2]++;
            }
            if(inputPassword[2] > '9')
            {
                inputPassword[2] = '0';
            }
        }
        key[2].keyShortFlag = 0;
    }
    if(key[3].keyShortFlag)
    {
        if(strstr((char*)inputPassword,"@") == NULL)
        {
            if(strcmp((char*)inputPassword,(char*)password) == 0)
        {
            view = 1;
            count = 0;
            LED1Flag = true;
        }
        else
        {
            count++;
            if(count >= 3)
            {
                count=0;
                LED2Flag = true;
            }
            strcpy((char*)inputPassword,"@@@");
        }

        }
        
        key[3].keyShortFlag = 0;
    }
}
char LED2Timer = 0;
char LED1Timer = 0;
char LED2State=0x00;
void LED_Task(void)
{
    if(LEDTaskTimer)return;
    LEDTaskTimer = LEDTaskPeriod;
    if(LED2Flag)
    {
        LED2Timer++;
        LED2State^=0x02;
        Led_Display(LED2State);
        if(LED2Timer == 50)
        {
            LED2Flag=false;
            LED2Timer=0;
            Led_Display(0x00);
        }
    }
    if(LED1Flag)
    {
        PWM_2K_OUT();
        LED1Timer++;
        Led_Display(0x01);
        if(LED1Timer == 50)
        {
            LED1Flag=false;
            LED1Timer=0;
            view=0;
            strcpy((char*)inputPassword,"@@@");
            Led_Display(0x00);
            PWM_1K_OUT();
        }
    }
}

void LCD_Task(void)
{
    if(LCDTaskTimer)return;
    LCDTaskTimer = LCDTaskPeriod;
    if(view == 0)
    {
        LCD_DisplayStringLine(Line1,(u8*)"       PSD           ");
        sprintf((char*)text,"    B1:%c            ",inputPassword[0]);
        LCD_DisplayStringLine(Line3,text);
        sprintf((char*)text,"    B2:%c            ",inputPassword[1]);
        LCD_DisplayStringLine(Line4,text);
        sprintf((char*)text,"    B3:%c            ",inputPassword[2]);
        LCD_DisplayStringLine(Line5,text);
    }
    else if(view == 1)
    {
        LCD_DisplayStringLine(Line1,(u8*)"       STA           ");
        sprintf((char*)text,"    F:%dHz           ",frac);
        LCD_DisplayStringLine(Line3,text);
        sprintf((char*)text,"    D:%d%%            ",D);
        LCD_DisplayStringLine(Line4,text);
        sprintf((char*)text,"                     ");
        LCD_DisplayStringLine(Line5,text);
    }
}

