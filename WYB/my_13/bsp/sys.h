#ifndef __HARDWARE_H__
#define __HARDWARE_H__
#include "main.h"
#include "usart.h"
#include "tim.h"
#include "stdbool.h"
#include "stdio.h"
#include "string.h"
typedef struct
{
    char keyMode;//按钮的模式
    bool keyState;//按钮的状态
    bool keyShortFlag;//按钮短按的标志
    uint32_t keyTime;//按钮被按下的时间
}keyFlag;


void LCD_Task(void);
void keyTask(void);
void taskRun(void);
void Sys_Init(void);
void keyFlagTask(void);
void LED_Task(void);
void USAERT_Task(void);

#define MAX_TIMER 5


#define LCDTaskTimer  sysTimer[0]
#define KEYTaskTimer  sysTimer[1]
#define USARTTTaskTimer  sysTimer[2]
#define LEDTaskTimer  sysTimer[3]
#define keyFlagTaskTimer sysTimer[4]

#define LCDTaskPeriod  50
#define KEYTaskPeriod  10
#define USARTTaskPeriod  10
#define LEDTaskPeriod  100
#define keyFlagTaskPeriod 0
#endif
