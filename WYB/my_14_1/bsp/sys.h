#ifndef __SYS__H__
#define __SYS__H__

#include "main.h"
#include "stdbool.h"
#define MAX_TIMER 6
#define Key_Timer sysTimer[0]
#define Key_Flag_Timer sysTimer[1]
#define LCD_Timer sysTimer[2]
#define LED_Timer sysTimer[3]
#define USART_Timer sysTimer[4]
#define ADC_Timer sysTimer[5]


#define Key_TimerPeriod 10 
#define Key_Flag_TimerPeriod 10
#define LCD_TimerPeriod  100
#define LED_TimerPeriod  100
#define USART_TimerPeriod 10
#define ADC_TimerPeriod 10


typedef struct
{
    char keyMode;
    bool keyState;
    bool key_Short_Falg;
}keyFlag;

void taskRun(void);
void sysInit(void);



#endif
