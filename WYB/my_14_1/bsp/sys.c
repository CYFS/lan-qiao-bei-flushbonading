#include "sys.h"
#include "lcd.h"
#include "usart.h"
#include "adc.h"
#include "tim.h"
#include "stdio.h"
#include "string.h"
u32 sysTimer[MAX_TIMER];
keyFlag key[4]={0,0,0};
u8 view = 0;
u8 text[21];
double V_Value = 0;
u32 frac = 1000;
double V_P1 = 0.3;
u8 USART_Data[64];
u8 USART_Pos = 0;
float D = 0.1;
u8 USART_RecTemp;
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart ->Instance == USART1)
	{
		USART_Timer = 10;
		USART_Data[USART_Pos ++] = USART_RecTemp;
		HAL_UART_Receive_IT(&huart1,&USART_RecTemp,1);
	}
}

void USAER_Task(void)
{
    if(USART_Timer)return;
    if(!USART_Pos)return;
    USART_Data[USART_Pos] = '\0';
    if(strlen((char*)USART_Data) == 1)
    {
        if(USART_Data[0]>= '0' && USART_Data[0] <= '9')
        {
            D = (USART_Data[0] - '0') / 10.0;
            TIM17->CCR1 = 80000000/80/frac * D;
        }
        else
        {
            sprintf((char*)text,"Error");
            HAL_UART_Transmit(&huart1,text,strlen((char*)text),50);
        }            
    }
    else
    {
            sprintf((char*)text,"Error");
            HAL_UART_Transmit(&huart1,text,strlen((char*)text),50);
    }
    USART_Pos = 0;
    memset(USART_Data,0,64);
}



void keyScTask(void)
{
    if(Key_Timer)return;
    Key_Timer = Key_TimerPeriod;
    key[0].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0);
    key[1].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1);
    key[2].keyState = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2);
    key[3].keyState = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0);
    for(u8 i = 0;i < 4;i++)
    {
        switch(key[i].keyMode)
        {
            case 0:
                if(key[i].keyState == 0)
                {
                    key[i].keyMode = 1;
                }
                break;
            case 1:
                if(key[i].keyState == 0)
                {
                    
                    key[i].keyMode = 2;
                }
                else
                {
                    key[i].keyMode = 0;
                }
                break;
            case 2:
                if(key[i].keyState == 1)
                {
                    key[i].key_Short_Falg = 1;
                    key[i].keyMode = 0;
                }
                break;
        }
    }
    
}

void keyFalgTask(void)
{
    if(Key_Flag_Timer)return;
    Key_Flag_Timer = Key_Flag_TimerPeriod;
    if(key[0].key_Short_Falg)
    {
        view = !view;
        key[0].key_Short_Falg = 0;
    }
    if(key[1].key_Short_Falg)
    {
        if(view == 1)
        {
            V_P1+=0.3;
            if(V_P1 > 3.3)V_P1 = 0;
            
        }
        key[1].key_Short_Falg = 0;
    }
    if(key[2].key_Short_Falg)
    {
        frac += 1000;
        if(frac > 10000) frac = 1000;
        TIM17->ARR = 80000000/80/frac;
        TIM17->CCR1 = 80000000/80/frac * D;
        key[2].key_Short_Falg = 0;
    }
    if(key[3].key_Short_Falg)
    {
        key[3].key_Short_Falg = 0;
    }
}

bool LED3_Flag = false;
void ADC_Task(void)
{
    if(ADC_Timer)return;
    ADC_Timer = ADC_TimerPeriod;
    HAL_ADC_Start(&hadc2);
    V_Value = HAL_ADC_GetValue(&hadc2) * 3.3 / 4096; 
    if(V_Value > V_P1)
    {
        LED3_Flag = true;
    }
    else
    {
        LED3_Flag = false;
    }
    
}
void LED_Disp(char pin)
{
    GPIOC->ODR = ~(pin << 8);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
}
char LED_State = 0x00;
void LED_Task(void)
{
    if(LED_Timer)return;
    LED_Timer = LED_TimerPeriod;
    if(view == 0)
    {
        LED_State=(LED_State|0x01)&0x05;
    }
    else if(view == 1)
    {
        LED_State=(LED_State|0x02)&0x06;
    }
    if(LED3_Flag)
    {
        LED_State^=0x04;
    }
    else
    {
        LED_State&=~(0X04);
    }
    LED_Disp(LED_State);
}
    
void LCD_Task(void)
{
    if(LCD_Timer)return;
    LCD_Timer = LCD_TimerPeriod;
    if(view == 0)
    {
        LCD_DisplayStringLine(Line2,(u8*)"        DATA        ");
        sprintf((char*)text,"     VR37:%.2fV     ",V_Value);
        LCD_DisplayStringLine(Line3,text);
        sprintf((char*)text,"     PA7:%dHz     ",frac);
        LCD_DisplayStringLine(Line4,text);
        
        
    }else if(view ==  1)
    {
        LCD_DisplayStringLine(Line2,(u8*)"        PARA        ");
        sprintf((char*)text,"     VP1:%.1fV     ",V_P1);
        LCD_DisplayStringLine(Line3,text);
        LCD_DisplayStringLine(Line4,(u8*)"                    ");
    }
}


void SysTick_Handler(void)
{
  
    HAL_IncTick();
    for(u8 i = 0;i < MAX_TIMER;i++)
    {
        if(sysTimer[i])sysTimer[i]--;
    }


}

void taskInit(void)
{
    LCD_Timer = LCD_TimerPeriod;
    Key_Timer = Key_TimerPeriod;
    Key_Flag_Timer = Key_Flag_TimerPeriod;
    ADC_Timer = ADC_TimerPeriod;
    USART_Timer = USART_TimerPeriod;
}


void sysInit(void)
{
    LCD_Init();
    LCD_Clear(Black);
    LCD_SetBackColor(Black);
    LCD_SetTextColor(White);
    LED_Disp(0x00);
    
    taskInit();
    HAL_TIM_PWM_Start_IT(&htim17,TIM_CHANNEL_1);
    HAL_ADCEx_Calibration_Start(&hadc2,ADC_SINGLE_ENDED);
    HAL_UART_Receive_IT(&huart1,&USART_RecTemp,1);
    
    
}

void taskRun(void)
{
    keyScTask();
    keyFalgTask();
    USAER_Task();
    ADC_Task();
    LED_Task();
    LCD_Task();
}

